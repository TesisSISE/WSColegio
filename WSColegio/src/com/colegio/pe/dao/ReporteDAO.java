package com.colegio.pe.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.colegio.pe.dao.interfaces.IReporteDAO;
import com.colegio.pe.util.Conexion;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.text.DecimalFormat;

public class ReporteDAO implements IReporteDAO {
	private static final Logger log = LogManager.getLogger(ReporteDAO.class);
	private static final DecimalFormat formatter;
	
	static {
		formatter = new DecimalFormat("#0.00");
	}
	
	@Override
	public ArrayList<Hashtable<String, Object>> aprobadosPorSeccion(int trimestre, String codProfesor) {
		ArrayList<Hashtable<String, Object>> lista = new ArrayList<Hashtable<String, Object>>();
		Hashtable<String, Object> item=null;
		Connection cn =Conexion.getInstance().getConnection();
		String query = "{call usp_reporte_aprobadosPorSalon(?,?)}";
		String query2 = "{call usp_seccion_buscarPorId(?)}";
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			
			cs.setInt(1, trimestre);
			cs.setString(2, codProfesor);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				item = new Hashtable<String, Object>();
				item.put("IdSeccion",rs.getString("IdSeccion"));
				item.put("DescripCurso",rs.getString("DescripCurso"));
				item.put("Aprobados",rs.getString("Aprobados"));
				item.put("Desaprobados",rs.getString("Desaprobados"));
				item.put("TotalAlumnos",rs.getString("TotalAlumnos"));
				
				int aprobados=Integer.parseInt(rs.getString("Aprobados"));
				int desaprobados=Integer.parseInt(rs.getString("Desaprobados"));
				int totalAlumnos=Integer.parseInt(rs.getString("TotalAlumnos"));
				
				double porcentajeAprobados=aprobados*100./totalAlumnos;
				double porcentajeDesaprobados=desaprobados*100./totalAlumnos;
				
				item.put("%Aprobados", formatter.format(porcentajeAprobados));
				item.put("%Desaprobados", formatter.format(porcentajeDesaprobados));
				
				CallableStatement cs2 = cn.prepareCall(query2);
				cs2.setInt(1, rs.getInt("IdSeccion"));
				
				ResultSet rs2 = cs2.executeQuery();
				
				if(rs2.next()) {
					item.put("CodGrado", rs2.getString("CodGrado"));
					item.put("NomSeccion", rs2.getString("NomSeccion"));
					item.put("Turno", rs2.getString("Turno"));
				}
				
				lista.add(item);
			}
			
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return lista;
	}

	@Override
	public ArrayList<Hashtable<String, Object>> progresoPorSeccion(int trimestre, String codProfesor) {
		ArrayList<Hashtable<String, Object>> lista = new ArrayList<Hashtable<String, Object>>();
		Hashtable<String, Object> item=null;
		Connection cn =Conexion.getInstance().getConnection();
		String query = "{call usp_reporte_progresoPorSalon(?,?)}";
		String query2 = "{call usp_seccion_buscarPorId(?)}";
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			
			cs.setInt(1, trimestre);
			cs.setString(2, codProfesor);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				item = new Hashtable<String, Object>();
				item.put("IdSeccion",rs.getString("IdSeccion"));
				item.put("DescripCurso",rs.getString("DescripCurso"));
				item.put("Logrado",rs.getString("Logrado"));
				item.put("Proceso",rs.getString("Proceso"));
				item.put("Deficiente",rs.getString("Deficiente"));
				item.put("TotalAlumnos",rs.getString("TotalAlumnos"));
				
				int logrado = rs.getInt("Logrado");
				int proceso = rs.getInt("Proceso");
				int deficiente = rs.getInt("Deficiente");
				int totalAlumnos = rs.getInt("TotalAlumnos");
				
				double porcentajeLogrado=logrado*100./totalAlumnos;
				double porcentajeProceso=proceso*100./totalAlumnos;
				double porcentajeDeficiente=deficiente*100./totalAlumnos;
				
				item.put("%Logrado", formatter.format(porcentajeLogrado));
				item.put("%Proceso", formatter.format(porcentajeProceso));
				item.put("%Deficiente", formatter.format(porcentajeDeficiente));
				
				CallableStatement cs2 = cn.prepareCall(query2);
				cs2.setInt(1, rs.getInt("IdSeccion"));
				
				ResultSet rs2 = cs2.executeQuery();
				
				if(rs2.next()) {
					item.put("CodGrado", rs2.getString("CodGrado"));
					item.put("NomSeccion", rs2.getString("NomSeccion"));
					item.put("Turno", rs2.getString("Turno"));
				}
				
				lista.add(item);
			}
			
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return lista;
	}

}
