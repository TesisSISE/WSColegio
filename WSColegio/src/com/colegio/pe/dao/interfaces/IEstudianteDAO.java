package com.colegio.pe.dao.interfaces;

import java.util.ArrayList;
import java.util.Hashtable;

public interface IEstudianteDAO {

	Hashtable<String,Object> registrarEstudiante(Hashtable<String,Object> estudiante);
	ArrayList<Hashtable<String,Object>> listarEstudiantes();
	ArrayList<Hashtable<String,Object>> listarEstudiantesFull();
	ArrayList<Hashtable<String,Object>> listarCursos(String codEstudiante);
	ArrayList<Hashtable<String, Object>> listarCursosWeb(String codEstudiante);
	ArrayList<Hashtable<String,Object>> listarProfesores(String codEstudiante);
	ArrayList<Hashtable<String, Object>> listarEstudiantesPrueba();
	Hashtable<String, Object> buscarPorDNI(String dniEstudiante);
	ArrayList<Hashtable<String, Object>> listarPorSeccion(int idSeccion);
	Hashtable<String,Object> modificarEstudiante(Hashtable<String,Object> estudiante);
	Hashtable<String,Object> eliminarEstudiante(String codEstudiante, boolean activado);
}
