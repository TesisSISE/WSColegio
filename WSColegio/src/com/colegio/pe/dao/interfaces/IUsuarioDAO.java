package com.colegio.pe.dao.interfaces;

import java.util.ArrayList;
import java.util.Hashtable;


public interface IUsuarioDAO {
	ArrayList<Hashtable<String, Object>> listarUsuarios();
	Hashtable<String, Object> login(String nombreUsuario, String clave);
	Hashtable<String, Object> cambiarClave(String codUsuario, String claveUsuario, String nuevaClaveusuario);
}
