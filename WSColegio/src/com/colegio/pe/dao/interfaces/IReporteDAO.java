package com.colegio.pe.dao.interfaces;

import java.util.ArrayList;
import java.util.Hashtable;

public interface IReporteDAO {
	ArrayList<Hashtable<String, Object>> aprobadosPorSeccion(int trimestre, String codProfesor);
	ArrayList<Hashtable<String, Object>> progresoPorSeccion(int trimestre, String codProfesor);

}
