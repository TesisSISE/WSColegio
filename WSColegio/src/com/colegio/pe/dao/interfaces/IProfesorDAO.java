package com.colegio.pe.dao.interfaces;

import java.util.ArrayList;
import java.util.Hashtable;

public interface IProfesorDAO {
	ArrayList<Hashtable<String,Object>> listarProfesores();
	Hashtable<String,Object> registrarProfesor(Hashtable<String,Object> profesor);
	ArrayList<Hashtable<String,Object>> listarSecciones(String codProfesor);
	ArrayList<Hashtable<String,Object>> listarCursosDeSeccion(int idSeccion, String codProfesor);
	Hashtable<String,Object> asociarCurso(ArrayList<Hashtable<String,Object>> profesores);
	Hashtable<String,Object> asignarSeccion(Hashtable<String,Object> profesor);
	ArrayList<Hashtable<String,Object>> listarAlumnos(int idSeccion);
	ArrayList<Hashtable<String,Object>> listarCursosImpartidos(String codProfesor, String nivel);
	Hashtable<String,Object> modificarProfesor(Hashtable<String,Object> profesor);
	Hashtable<String,Object> eliminarProfesor(String codProfesor, boolean activado);
}
