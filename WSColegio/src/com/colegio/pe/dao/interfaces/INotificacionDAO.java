package com.colegio.pe.dao.interfaces;

import java.util.ArrayList;
import java.util.Hashtable;

public interface INotificacionDAO {
	ArrayList<Hashtable<String,Object>> listarNotificaciones();
	ArrayList<Hashtable<String,Object>> listarNotificacionesPorApoderado(String codApoderado);
}
