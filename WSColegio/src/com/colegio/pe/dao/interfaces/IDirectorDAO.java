package com.colegio.pe.dao.interfaces;

import java.util.ArrayList;
import java.util.Hashtable;

public interface IDirectorDAO {
	ArrayList<Hashtable<String,Object>> listarCursos();
	Hashtable<String,Object> registrarDirector(Hashtable<String,Object> director);
	Hashtable<String,Object> aperturarSeccion(Hashtable<String,Object> seccion);

}
