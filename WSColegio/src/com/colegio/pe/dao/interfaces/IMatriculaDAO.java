package com.colegio.pe.dao.interfaces;

import java.util.Hashtable;

public interface IMatriculaDAO {
	Hashtable<String, Object> matricularEstudiante(Hashtable<String, Object> estudiante);
}
