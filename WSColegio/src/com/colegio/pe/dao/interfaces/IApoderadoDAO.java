package com.colegio.pe.dao.interfaces;

import java.util.ArrayList;
import java.util.Hashtable;

public interface IApoderadoDAO {
	ArrayList<Hashtable<String, Object>> listarApoderados();
	ArrayList<Hashtable<String, Object>> listarEstudiantes(String codApoderado);
	Hashtable<String, Object> registrarApoderado(Hashtable<String, Object> apoderado);
	Hashtable<String, Object> asociarEstudiante(Hashtable<String, Object> estudiante);
	Hashtable<String, Object> modificarApoderado(Hashtable<String, Object> apoderado);
	Hashtable<String, Object> eliminarApoderado(String codApoderado, boolean activado);
}
