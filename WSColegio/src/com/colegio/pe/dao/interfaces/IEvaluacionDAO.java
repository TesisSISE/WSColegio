package com.colegio.pe.dao.interfaces;

import java.util.ArrayList;
import java.util.Hashtable;

public interface IEvaluacionDAO {
	ArrayList<Hashtable<String,Object>> listarTipoEvaluacionPorCurso(String codCurso);
	Hashtable<String,Object> registrarNotas(ArrayList<Hashtable<String,Object>> notas);
	ArrayList<Hashtable<String,Object>> listarNotasPorDetalleCriterio(int idSeccion, String codCurso, String codProfesor, String codTipoEvaluacion);
	Hashtable<String,Object> registrarTipoEvaluacion(String descripTipoEvaluacion, int porcentaje, String codCurso);
	ArrayList<Hashtable<String,Object>> recordTrimestral(String codEstudiante, int trimestre);
	ArrayList<Hashtable<String,Object>> recordTotal(String codEstudiante);
	ArrayList<Hashtable<String,Object>> promedioPorCriterio(String codEstudiante, String codCurso);
	ArrayList<Hashtable<String,Object>> promedioPorCriterioTrimestral(String codEstudiante, String codCurso, int trimestre);
	ArrayList<Hashtable<String,Object>> detalleNota(String codEstudiante, String codCurso, int trimestre, String codTipoEvaluacion);
	

}
