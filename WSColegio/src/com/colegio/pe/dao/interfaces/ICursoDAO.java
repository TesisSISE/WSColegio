package com.colegio.pe.dao.interfaces;

import java.util.ArrayList;
import java.util.Hashtable;

public interface ICursoDAO {
	ArrayList<Hashtable<String, Object>> listarCursos();
	Hashtable<String, Object> registrarCurso(Hashtable<String, Object> curso);
	ArrayList<Hashtable<String, Object>> listarCursosSecundaria();
	Hashtable<String, Object> modificarCurso(Hashtable<String, Object> curso);
	Hashtable<String, Object> eliminarCurso(String codCurso, boolean activado);
	
}
