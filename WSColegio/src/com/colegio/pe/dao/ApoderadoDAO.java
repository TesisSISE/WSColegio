package com.colegio.pe.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.colegio.pe.dao.interfaces.IApoderadoDAO;
import com.colegio.pe.util.Conexion;

import java.sql.CallableStatement;
import java.sql.Connection;

public class ApoderadoDAO implements IApoderadoDAO {
	private static final Logger log = LogManager.getLogger(ApoderadoDAO.class);
	
	@Override
	public ArrayList<Hashtable<String, Object>> listarApoderados() {
		ArrayList<Hashtable<String, Object>> lista = new ArrayList<Hashtable<String, Object>>();
		Hashtable<String, Object> apoderado=null;
		Connection cn =Conexion.getInstance().getConnection();
		String query = "{call usp_apoderado_listar()}";
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				apoderado = new Hashtable<String, Object>();
				apoderado.put("codApoderado",rs.getString("CodApoderado"));
				apoderado.put("nomApoderado",rs.getString("NomApoderado"));
				apoderado.put("apePaternoApoderado",rs.getString("ApePaternoApoderado"));
				apoderado.put("apeMaternoApoderado",rs.getString("ApeMaternoApoderado"));
				apoderado.put("DirApoderado",rs.getString("DirApoderado"));
				apoderado.put("TelfApoderado",rs.getString("TelfApoderado"));
				apoderado.put("SexoApoderado",rs.getString("SexoApoderado"));
				apoderado.put("FechaNacApoderado",rs.getString("FechaNacApoderado"));
				apoderado.put("DniApoderado",rs.getString("DniApoderado"));
				apoderado.put("FotoApoderado",rs.getString("FotoApoderado")==null?"":rs.getString("FotoApoderado"));
				apoderado.put("EstadoApoderado",rs.getBoolean("EstadoApoderado"));
				
				lista.add(apoderado);
			}
			
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {	
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return lista;
	}

	@Override
	public ArrayList<Hashtable<String, Object>> listarEstudiantes(String codApoderado) {
		ArrayList<Hashtable<String, Object>> lista = new ArrayList<Hashtable<String, Object>>();
		Hashtable<String, Object> estudiante=null;
		
		Connection cn = Conexion.getInstance().getConnection();
		String query = "{call usp_apoderado_listarEstudiantesPorApoderado(?)}";
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			
			cs.setString(1, codApoderado);
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				estudiante = new Hashtable<String, Object>();
				Character sexo =rs.getString("SexoEstudiante")==null?null:rs.getString("SexoEstudiante").charAt(0);
				
				estudiante.put("CodEstudiante", rs.getString("CodEstudiante"));
				estudiante.put("NomEstudiante", rs.getString("NomEstudiante"));
				estudiante.put("ApePaternoEstudiante", rs.getString("ApePaternoEstudiante"));
				estudiante.put("ApeMaternoEstudiante", rs.getString("ApeMaternoEstudiante"));
				estudiante.put("FechaNacEstudiante", rs.getDate("FechaNacEstudiante"));
				estudiante.put("CodEstudiante", rs.getString("CodEstudiante"));
				estudiante.put("CodEstudiante", rs.getString("CodEstudiante"));	
				estudiante.put("SexoEstudiante", sexo);
				estudiante.put("DniEstudiante", rs.getString("DniEstudiante"));
				estudiante.put("Parentesco", rs.getString("Parentesco"));
				estudiante.put("CodGrado", rs.getString("CodGrado"));
				estudiante.put("NomSeccion", rs.getString("NomSeccion"));
				estudiante.put("EstadoEstudiante", rs.getBoolean("EstadoEstudiante"));
				
				lista.add(estudiante);
			}
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		return lista;
	}


	@Override
	public Hashtable<String, Object> asociarEstudiante(Hashtable<String, Object> estudiante) {
		Hashtable<String, Object> resultado = new Hashtable<String, Object>();
		String query = "{call usp_apoderado_estudiante_asociar(?,?,?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
		
			cs.setString(1, estudiante.get("codApoderado").toString());
			cs.setString(2, estudiante.get("codEstudiante").toString());
			cs.setString(3, estudiante.get("parentesco").toString());
				
			
			boolean registrado = cs.executeUpdate()>0;
			
			if(registrado) {
				resultado.put("resultado", "¡Asociacion realizada exitosamente!");
			}else{
				resultado.put("resultado", "No se pudo asociar");
			}
		
		} catch (SQLException e) {
			resultado.put("resultado", "No se pudo asociar el estudiante : "+e.getMessage());
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			resultado.put("resultado", "No se pudo asociar el estudiante : "+e.getMessage());
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		
		
		return resultado;
	}

	@Override
	public Hashtable<String, Object> registrarApoderado(Hashtable<String, Object> apoderado) {
		Hashtable<String, Object> resultado = new Hashtable<String, Object>();
		
		Connection cn = Conexion.getInstance().getConnection();
		String query = "{call usp_apoderado_registrar(?,?,?,?,?,?,?,?,?)}";
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
		
			cs.setString(1, apoderado.get("nomApoderado").toString());
			cs.setString(2, apoderado.get("apePaternoApoderado").toString());
			cs.setString(3, apoderado.get("apeMaternoApoderado").toString());
			cs.setString(4, apoderado.get("dirApoderado").toString());
			cs.setString(5, apoderado.get("telfApoderado").toString());
			cs.setString(6, apoderado.get("sexoApoderado").toString());
			cs.setString(7, apoderado.get("fechaNacApoderado").toString());
			cs.setString(8, apoderado.get("dniApoderado").toString());
			cs.setString(9, apoderado.get("fotoApoderado").toString());
		
			boolean registrado = cs.executeUpdate()>0;
			
			if(registrado) {
				resultado.put("resultado","Apoderado registrado correctamente!");
			}else {
				resultado.put("resultado","No se pudo registrar!");
			}
			
		} catch (SQLException e) {
			resultado.put("resultado","No se pudo registrar : "+e.getMessage());
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			resultado.put("resultado","No se pudo registrar : "+e.getMessage());
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return resultado;
	}

	@Override
	public Hashtable<String, Object> modificarApoderado(Hashtable<String, Object> apoderado) {
		Hashtable<String, Object> resultado = new Hashtable<String, Object>();
		
		Connection cn = Conexion.getInstance().getConnection();
		String query = "{call usp_apoderado_modificar(?,?,?,?,?,?,?,?,?,?)}";
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
		
			cs.setString(1, apoderado.get("CodApoderado").toString());
			cs.setString(2, apoderado.get("NomApoderado").toString());
			cs.setString(3, apoderado.get("ApePaternoApoderado").toString());
			cs.setString(4, apoderado.get("ApeMaternoApoderado").toString());
			cs.setString(5, apoderado.get("DirApoderado").toString());
			cs.setString(6, apoderado.get("TelfApoderado").toString());
			cs.setString(7, apoderado.get("SexoApoderado").toString());
			cs.setString(8, apoderado.get("FechaNacApoderado").toString());
			cs.setString(9, apoderado.get("DniApoderado").toString());
			cs.setString(10, apoderado.get("FotoApoderado").toString());
			
		
			boolean actualizado = cs.executeUpdate()>0;
			
			if(actualizado) {
				resultado.put("resultado","¡Apoderado modificado!");
			}else {
				resultado.put("resultado","No se pudo modificar!");
			}
			
		} catch (SQLException e) {
			resultado.put("resultado","No se pudo modificar : "+e.getMessage());
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			resultado.put("resultado","Error general : "+e.getMessage());
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return resultado;
	}

	@Override
	public Hashtable<String, Object> eliminarApoderado(String codApoderado, boolean activado) {
		Hashtable<String, Object> resultado = new Hashtable<String, Object>();
		
		Connection cn = Conexion.getInstance().getConnection();
		String query = "{call usp_apoderado_eliminar(?,?)}";
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
		
			cs.setString(1, codApoderado);
			cs.setBoolean(2, activado);
		
			boolean actualizado = cs.executeUpdate()>0;
			
			if(actualizado) {
				if(activado) {
					resultado.put("resultado","Activado");
				}else {
					resultado.put("resultado","Desactivado");				
				}
			}else {
				resultado.put("resultado","¡No se pudo eliminar!");
			}
			
		} catch (SQLException e) {
			resultado.put("resultado","No se pudo eliminar : "+e);
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			resultado.put("resultado","Error general : "+e);
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return resultado;
	}

}
