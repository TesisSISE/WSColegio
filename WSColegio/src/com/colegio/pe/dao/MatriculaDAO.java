package com.colegio.pe.dao;

import java.sql.SQLException;
import java.util.Hashtable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.colegio.pe.dao.interfaces.IMatriculaDAO;
import com.colegio.pe.util.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;

public class MatriculaDAO implements IMatriculaDAO{
	private static final Logger log = LogManager.getLogger(MatriculaDAO.class);
	
	@Override
	public Hashtable<String, Object> matricularEstudiante(Hashtable<String, Object> estudiante) {
		Hashtable<String, Object> resultado = new Hashtable<String, Object>();
		String query = "{call usp_matricula_registrar(?,?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
		
			cs.setString(1, estudiante.get("codEstudiante").toString());
			cs.setInt(2, Integer.parseInt(estudiante.get("idSeccion").toString()));
			
			
			boolean matriculado = cs.executeUpdate()>0;
			
			if(matriculado) {
				resultado.put("resultado", "¡Matriculado!");
			}else {
				resultado.put("resultado", "No se pudo matricular");
			}
			
		
		} catch (SQLException e) {
			resultado.put("resultado", "No se pudo matricular : "+e.getMessage());
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			resultado.put("resultado", "No se pudo matricular : "+e.getMessage());
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		
		return resultado;
	}

}
