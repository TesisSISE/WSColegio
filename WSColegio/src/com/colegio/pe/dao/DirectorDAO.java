package com.colegio.pe.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.colegio.pe.dao.interfaces.IDirectorDAO;
import com.colegio.pe.util.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;

public class DirectorDAO implements IDirectorDAO {
	private static final Logger log = LogManager.getLogger(DirectorDAO.class);
	
	@Override
	public ArrayList<Hashtable<String, Object>> listarCursos() {
		ArrayList<Hashtable<String, Object>> lista = new ArrayList<Hashtable<String, Object>>();
		Hashtable<String, Object> curso = null;
		String query = "{call usp_director_listarCursos()}";
		Connection cn = Conexion.getInstance().getConnection();
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				curso = new Hashtable<String, Object>();
				curso.put("DescripCurso", rs.getString("DescripCurso"));
				lista.add(curso);
			}
			
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		
		return lista;
	}

	@Override
	public Hashtable<String, Object> registrarDirector(Hashtable<String, Object> director) {
		Hashtable<String, Object> resultado = new Hashtable<String, Object>();
		String query = "{call usp_director_registrar(?,?,?,?,?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
		
			cs.setString(1, director.get("nomDirector").toString());
			cs.setString(2, director.get("apePaternoDirector").toString());
			cs.setString(3, director.get("apeMaternoDirector").toString());
			cs.setString(4, director.get("dniDirector").toString());
			cs.setString(5, director.get("sexoDirector").toString());
			
			boolean registrado = cs.executeUpdate()>0;
			
			if(registrado) {
				resultado.put("resultado", "¡Director registrado!");
			}else {
				resultado.put("resultado", "No se pudo registrar el director");
			}
			
		} catch (SQLException e) {
			resultado.put("resultado", "No se pudo registrar el director : "+e.getMessage());
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			resultado.put("resultado", "No se pudo registrar el director : "+e.getMessage());
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		
		return resultado;
	}

	@Override
	public Hashtable<String, Object> aperturarSeccion(Hashtable<String, Object> seccion) {
		Hashtable<String, Object> resultado = new Hashtable<String, Object>();
		String query = "{call usp_seccion_aperturar(?,?,?,?,?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			
			cs.setString(1, seccion.get("turno").toString());
			cs.setString(2, seccion.get("codGrado").toString());
			cs.setString(3, seccion.get("nomSeccion").toString());
			cs.setString(4, seccion.get("estadoSeccion").toString());
			cs.setString(5, seccion.get("codAula").toString());
			
			boolean aperturado = cs.executeUpdate()>0;
			
			if(aperturado) {
				resultado.put("resultado", "¡Seccion aperturada con exito!");
			}else {
				resultado.put("resultado", "No se pudo aperturar la seccion");
			}
			
			
			
		} catch (SQLException e) {
			resultado.put("resultado", "No se pudo aperturar la seccion : "+e.getMessage());
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			resultado.put("resultado", "No se pudo aperturar la seccion : "+e.getMessage());
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return resultado;
	}
	
}
