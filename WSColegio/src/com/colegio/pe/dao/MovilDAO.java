package com.colegio.pe.dao;

import java.sql.SQLException;
import java.util.Hashtable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.colegio.pe.dao.interfaces.IMovilDAO;
import com.colegio.pe.util.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;

public class MovilDAO implements IMovilDAO{
	private static final Logger log = LogManager.getLogger(MovilDAO.class);
	
	
	@Override
	public Hashtable<String, Object> registrarMovil(Hashtable<String, Object> movil) {
		Hashtable<String, Object> resultado = new Hashtable<String, Object>();
		Connection cn = Conexion.getInstance().getConnection();
		String query = "{call usp_movil_registrar(?,?,?)}";
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			
			cs.setString(1, movil.get("idMovil").toString());
			cs.setString(2, movil.get("token").toString());
			cs.setString(3, movil.get("codUsuario").toString());
			
			boolean registrado = cs.executeUpdate()>0;
			
			if(registrado) {
				resultado.put("resultado", "Movil Registrado");
			}else {
				resultado.put("resultado", "No se pudo registrar el movil");
			}
			
		} catch (SQLException e) {
			resultado.put("resultado", "error SQL:"+e.getMessage());
			log.debug("error SQL: ", e);
			e.printStackTrace();
		}
		  catch (Exception e) {
			resultado.put("resultado", "error generico:"+e.getMessage());
			log.debug("error generico: ", e);
			e.printStackTrace();
		  }
		finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return resultado;
	}

}
