package com.colegio.pe.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.colegio.pe.dao.interfaces.IProfesorDAO;
import com.colegio.pe.util.Conexion;

import java.sql.CallableStatement;
import java.sql.Connection;

public class ProfesorDAO implements IProfesorDAO{
	private static final Logger log = LogManager.getLogger(ProfesorDAO.class);
	
	@Override
	public ArrayList<Hashtable<String,Object>> listarProfesores() {
		ArrayList<Hashtable<String,Object>> lista = new ArrayList<Hashtable<String,Object>>();
		Hashtable<String,Object> profesor = null;
		String query = "{call usp_profesor_listar()}";
		Connection cn = Conexion.getInstance().getConnection();
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				profesor = new Hashtable<String,Object>();
				profesor.put("codProfesor",rs.getString("CodProfesor"));
				profesor.put("nomProfesor",rs.getString("NomProfesor"));
				profesor.put("apePaternoProfesor",rs.getString("ApePaternoProfesor"));
				profesor.put("apeMaternoProfesor",rs.getString("ApeMaternoProfesor"));
				profesor.put("sexoProfesor",rs.getString("SexoProfesor").charAt(0));
				profesor.put("dirProfesor",rs.getString("DirProfesor"));
				profesor.put("emailProfesor",rs.getString("EmailProfesor"));
				profesor.put("telfProfesor",rs.getString("TelfProfesor"));
				profesor.put("fechaNacProfesor",rs.getDate("FechaNacProfesor"));
				profesor.put("EstadoProfesor", rs.getBoolean("EstadoProfesor"));
				
				lista.add(profesor);
			}
		
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		
		
		return lista;
	}


	@Override
	public ArrayList<Hashtable<String, Object>> listarSecciones(String codProfesor) {
		ArrayList<Hashtable<String,Object>> lista = new ArrayList<Hashtable<String,Object>>();
		Hashtable<String,Object> seccion;
		String query = "{call usp_profesor_listarSecciones(?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
		
			cs.setString(1, codProfesor);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				seccion = new Hashtable<String,Object>();
				seccion.put("IdSeccion", rs.getInt("IdSeccion"));
				seccion.put("GradoSeccion", rs.getString("GradoSeccion"));
				seccion.put("Turno", rs.getString("Turno"));
				
				lista.add(seccion);
			}
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		return lista;
	}

	@Override
	public ArrayList<Hashtable<String, Object>> listarCursosDeSeccion(int idSeccion, String codProfesor) {
		ArrayList<Hashtable<String,Object>> lista = new ArrayList<Hashtable<String,Object>>();
		Hashtable<String,Object> curso;
		String query = "{call usp_profesor_listarCursosDeSecciones(?,?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
		
			cs.setInt(1, idSeccion);
			cs.setString(2, codProfesor);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				curso = new Hashtable<String,Object>();
				curso.put("CodCurso", rs.getString("CodCurso"));
				curso.put("DescripCurso", rs.getString("DescripCurso"));
				curso.put("EstadoCurso", rs.getBoolean("EstadoCurso"));
				curso.put("ImagenCurso", rs.getString("ImagenCurso"));
				
				lista.add(curso);
			}
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		return lista;
	}


	@Override
	public Hashtable<String, Object> registrarProfesor(Hashtable<String, Object> profesor) {
		Hashtable<String, Object> resultado = new Hashtable<String, Object>();
		String query = "{call usp_profesor_registrar(?,?,?,?,?,?,?,?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			
			cs.setString(1, profesor.get("nomProfesor").toString());
			cs.setString(2, profesor.get("apePaternoProfesor").toString());
			cs.setString(3, profesor.get("apeMaternoProfesor").toString());
			cs.setString(4, profesor.get("sexoProfesor").toString());
			cs.setString(5, profesor.get("dirProfesor").toString());
			cs.setString(6, profesor.get("emailProfesor").toString());
			cs.setString(7, profesor.get("telfProfesor").toString());
			cs.setString(8, profesor.get("fechaNacProfesor").toString());
		
			boolean registrado = cs.executeUpdate()>0;
			
			if(registrado) {
				resultado.put("resultado", "¡Profesor registrado!");
			}else {
				resultado.put("resultado", "No se pudo registrar");
			}
			
		} catch (SQLException e) {
			resultado.put("resultado", "No se pudo registrar el profesor : "+e.getMessage());
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			resultado.put("resultado", "No se pudo registrar el profesor : "+e.getMessage());
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return resultado;
	}


	@Override
	public Hashtable<String, Object> asociarCurso(ArrayList<Hashtable<String,Object>> profesores) {
		Hashtable<String, Object> resultado = new Hashtable<String, Object>();
		String query = "{call usp_profesor_curso_asociar(?,?,?,?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			boolean cursoAsociado=false;
			for(Hashtable<String,Object> profesor: profesores) {
				cs.setString(1, profesor.get("codProfesor").toString());
				cs.setString(2, profesor.get("codCurso")==null?"":profesor.get("codCurso").toString());
				cs.setString(3, profesor.get("codGrado")==null?"S":profesor.get("codGrado").toString());
				
				boolean estadoProfesorCurso = profesor.get("estadoProfesorCurso").toString().equals("1")?true:false;
				
				cs.setBoolean(4, new Boolean(estadoProfesorCurso));
				
				cursoAsociado = cs.executeUpdate()>0;
				if(cursoAsociado==false)break;
			}
			
			
			
			if(cursoAsociado) {
				resultado.put("resultado", "¡Se asociaron los cursos al profesor!");
			}else {
				resultado.put("resultado", "No se pudo asociar curso");
			}
			
		} catch (SQLException e) {
			resultado.put("resultado", "No se pudo asociar el curso : "+e.getMessage());
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			resultado.put("resultado", "No se pudo asociar el curso : "+e.getMessage());
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return resultado;
	}


	@Override
	public Hashtable<String, Object> asignarSeccion(Hashtable<String, Object> profesor) {
		Hashtable<String, Object> resultado = new Hashtable<String, Object>();
		String query = "{call usp_profesor_curso_seccion_asignar(?,?,?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
		
			cs.setString(1, profesor.get("codProfesor").toString());
			cs.setString(2, profesor.get("codCurso").toString());
			cs.setInt(3, Integer.parseInt(profesor.get("idSeccion").toString()));
			
			
			boolean seccionAsignada = cs.executeUpdate()>0;
			
			if(seccionAsignada) {
				resultado.put("resultado", "¡Seccion asignada!");
			}else {
				resultado.put("resultado", "No se pudo asignar la seccion");
			}
			
		} catch (SQLException e) {
			resultado.put("resultado", "No se pudo asignar la seccion : "+e.getMessage());
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			resultado.put("resultado", "No se pudo asignar la seccion : "+e.getMessage());
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return resultado;
	}


	@Override
	public ArrayList<Hashtable<String, Object>> listarAlumnos(int idSeccion) {
		ArrayList<Hashtable<String,Object>> lista = new ArrayList<Hashtable<String,Object>>();
		Hashtable<String,Object> alumno;
		String query = "{call usp_profesor_listarAlumnos(?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
		
			cs.setInt(1, idSeccion);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				alumno = new Hashtable<String,Object>();
				alumno.put("CodApoderado", rs.getString("CodApoderado"));
				alumno.put("NomApoderado", rs.getString("NomApoderado"));
				alumno.put("ApePaternoApoderado", rs.getString("ApePaternoApoderado"));
				alumno.put("ApeMaternoApoderado", rs.getString("ApeMaternoApoderado"));
				alumno.put("TelfApoderado", rs.getString("TelfApoderado"));
				alumno.put("DniApoderado", rs.getString("DniApoderado"));
				alumno.put("CodEstudiante", rs.getString("CodEstudiante"));
				alumno.put("NomEstudiante", rs.getString("NomEstudiante"));
				alumno.put("ApePaternoEstudiante", rs.getString("ApePaternoEstudiante"));
				alumno.put("ApeMaternoEstudiante", rs.getString("ApeMaternoEstudiante"));
				alumno.put("DniEstudiante", rs.getString("DniEstudiante"));
				alumno.put("FotoEstudiante", rs.getString("FotoEstudiante")==null?"":rs.getString("FotoEstudiante"));
				alumno.put("EstadoEstudiante", rs.getBoolean("EstadoEstudiante"));
				
				lista.add(alumno);
			}
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		return lista;
	}


	@Override
	public ArrayList<Hashtable<String, Object>> listarCursosImpartidos(String codProfesor, String nivel) {
		ArrayList<Hashtable<String,Object>> lista = new ArrayList<Hashtable<String,Object>>();
		Hashtable<String,Object> cursoImpartido;
		String query = "{call usp_profesor_cursosImpartidos(?,?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
		
			cs.setString(1, codProfesor);
			cs.setString(2, nivel);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				cursoImpartido = new Hashtable<String,Object>();
				if(nivel.equals("P")) {
					cursoImpartido.put("CodGrado", rs.getString("CodGrado"));
				}else if(nivel.equals("S")) {
					cursoImpartido.put("CodCurso", rs.getString("CodCurso"));
				}
				
				cursoImpartido.put("EstadoProfesorCurso", rs.getString("EstadoProfesorCurso"));
				
				lista.add(cursoImpartido);
			}
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		return lista;
	}


	@Override
	public Hashtable<String, Object> modificarProfesor(Hashtable<String, Object> profesor) {
		Hashtable<String, Object> resultado = new Hashtable<String, Object>();
		String query = "{call usp_profesor_modificar(?,?,?,?,?,?,?,?,?,?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			
			cs.setString(1, profesor.get("codProfesor").toString());
			cs.setString(2, profesor.get("nomProfesor").toString());
			cs.setString(3, profesor.get("apePaternoProfesor").toString());
			cs.setString(4, profesor.get("apeMaternoProfesor").toString());
			cs.setString(5, profesor.get("sexoProfesor").toString());
			cs.setString(6, profesor.get("dirProfesor").toString());
			cs.setString(7, profesor.get("emailProfesor").toString());
			cs.setString(8, profesor.get("telfProfesor").toString());
			cs.setString(9, profesor.get("fechaNacProfesor").toString());
			cs.setString(10, profesor.get("fotoProfesor").toString());
		
			boolean actualizado = cs.executeUpdate()>0;
			
			if(actualizado) {
				resultado.put("resultado", "¡Profesor modificar!");
			}else {
				resultado.put("resultado", "No se pudo modificar");
			}
			
		} catch (SQLException e) {
			resultado.put("resultado", "No se pudo modificar el profesor : "+e.getMessage());
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			resultado.put("resultado", "No se pudo modificar el profesor : "+e.getMessage());
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return resultado;
	}


	@Override
	public Hashtable<String, Object> eliminarProfesor(String codProfesor, boolean activado) {
		Hashtable<String, Object> resultado = new Hashtable<String, Object>();
		String query = "{call usp_profesor_eliminar(?,?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			
			cs.setString(1, codProfesor);
			cs.setBoolean(2, activado);
		
			boolean actualizado = cs.executeUpdate()>0;
			
			if(actualizado) {
				if(activado) {
					resultado.put("resultado", "Activado");
				}else {
					resultado.put("resultado", "Desactivado");
				}
			}else {
				resultado.put("resultado", "No se pudo eliminar");
			}
			
		} catch (SQLException e) {
			resultado.put("resultado", "No se pudo eliminar el profesor : "+e);
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			resultado.put("resultado", "No se pudo eliminar el profesor : "+e);
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return resultado;
	}

	
}
