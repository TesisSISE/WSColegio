package com.colegio.pe.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.colegio.pe.dao.interfaces.IEvaluacionDAO;
import com.colegio.pe.util.Conexion;
import com.colegio.pe.util.GCMSender;
import java.sql.CallableStatement;
import java.sql.Connection;

public class EvaluacionDAO implements IEvaluacionDAO{
	private static final Logger log = LogManager.getLogger(EvaluacionDAO.class);
	
	@Override
	public ArrayList<Hashtable<String, Object>> listarTipoEvaluacionPorCurso(String codCurso) {
		ArrayList<Hashtable<String, Object>> lista = new ArrayList<Hashtable<String, Object>>();
		Hashtable<String, Object> tipoEvaluacion = null;
		String query = "{call usp_tipo_evaluacion_listarPorCurso(?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
		
			cs.setString(1, codCurso);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				tipoEvaluacion = new Hashtable<String, Object>();
				tipoEvaluacion.put("CodTipoEvaluacion", rs.getString("CodTipoEvaluacion"));
				tipoEvaluacion.put("DescripTipoEvaluacion", rs.getString("DescripTipoEvaluacion"));
				//tipoEvaluacion.put("Porcentaje", rs.getInt("Porcentaje"));
				
				
				lista.add(tipoEvaluacion);
			}
			
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
			
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		
		
		return lista;
	}

	private Hashtable<String, Object> notificacion;
	@Override
	public Hashtable<String, Object> registrarNotas(ArrayList<Hashtable<String, Object>> notas) {
		Hashtable<String, Object> resultado = new Hashtable<String, Object>();
		notificacion = null;
		String query = "{call usp_evaluacion_registrarNota(?,?,?,?,?,?,?,?)}";
		ResultSet rs = null;
		Connection cn = Conexion.getInstance().getConnection();
		
		try {
			cn.setAutoCommit(false);
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
		
			for(Hashtable<String, Object> elemento: notas) {
				cs.setString(1, elemento.get("descripEvaluacion").toString());
				cs.setDouble(2, Double.parseDouble(elemento.get("nota").toString()));
				cs.setString(3, elemento.get("codEstudiante").toString());
				cs.setString(4, elemento.get("codTipoEvaluacion").toString());
				cs.setString(5, elemento.get("codProfesor").toString());
				cs.setString(6, elemento.get("codCurso").toString());
				cs.setInt(7, Integer.parseInt(elemento.get("idSeccion").toString()));
				cs.setInt(8, Integer.parseInt(elemento.get("idEvaluacion").toString()));
				
				rs = cs.executeQuery();
			
				//Enviando notificacion a todos los dispositivos con el mismo usuario
				while(rs.next()) {
					//Enviar notificacion
					notificacion = new Hashtable<String, Object>();
					notificacion.put("IdNotificacion", rs.getInt("IdNotificacion"));
					notificacion.put("IdEvaluacion", rs.getInt("IdEvaluacion"));
					notificacion.put("CodProfesor", rs.getString("CodProfesor"));
					notificacion.put("CodApoderado", rs.getString("CodApoderado"));
					notificacion.put("CodEstudiante", rs.getString("CodEstudiante"));
					notificacion.put("Mensaje", rs.getString("Mensaje"));
					notificacion.put("FechaNotificacion", rs.getString("FechaNotificacion"));
					notificacion.put("EstadoNotificacion", rs.getString("EstadoNotificacion"));
					notificacion.put("Token",rs.getString("Token")==null?"":rs.getString("Token"));
					notificacion.put("CodUsuario",rs.getString("CodUsuario")==null?"":rs.getString("CodUsuario"));
					notificacion.put("CodCurso",rs.getString("CodCurso")==null?"":rs.getString("CodCurso"));
					notificacion.put("DescripCurso",rs.getString("DescripCurso")==null?"":rs.getString("DescripCurso"));
					notificacion.put("Trimestre",rs.getString("Trimestre")==null?"":rs.getString("trimestre"));
					notificacion.put("CodTipoEvaluacion",rs.getString("CodTipoEvaluacion")==null?"":rs.getString("CodTipoEvaluacion"));
					
					//Cuando se envie la notificacion cambiamos su estado
									
					String token = notificacion.get("Token").toString();
					
					if(token!=null && !token.isEmpty()) {
						
						new Thread() {

							@Override
							public void run() {
								String response = new GCMSender().sender(notificacion);
							}
							
						}.start();
								
						//tambien se podria enviar correo
					}
	
				}
				
			}
		
			cn.commit();
			resultado.put("respuesta", "Notas registradas");
		} catch (SQLException e) {
			resultado.put("respuesta", e.getMessage());
			log.debug("error SQL: ", e);
			e.printStackTrace();
			
		} catch (Exception e) {
			resultado.put("respuesta", e.getMessage());
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}	
		
		return resultado;
	}

	@Override
	public ArrayList<Hashtable<String, Object>> listarNotasPorDetalleCriterio(
			int idSeccion, String codCurso, String codProfesor,
			String codTipoEvaluacion) {
		ArrayList<Hashtable<String, Object>> lista = new ArrayList<Hashtable<String, Object>>();
		ArrayList<Hashtable<String, Object>> listaNotas = null;
		String query1 = "{call usp_estudiante_listarPorSeccion(?)}";
		String query2 = "{call usp_profesor_listarNotas(?,?,?,?,?)}";
		Connection cn = Conexion.getInstance().getConnection();
		Hashtable<String, Object> estudiante = null;
		Hashtable<String, Object> nota = null;
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query1);
			CallableStatement cs2 = null;
			
			cs.setInt(1, idSeccion);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				estudiante = new Hashtable<String, Object>();
				String codEstudiante=rs.getString("CodEstudiante");
				estudiante.put("CodEstudiante", codEstudiante);
				estudiante.put("NomEstudiante", rs.getString("NomEstudiante"));
				estudiante.put("ApePaternoEstudiante", rs.getString("ApePaternoEstudiante"));
				estudiante.put("ApeMaternoEstudiante", rs.getString("ApeMaternoEstudiante"));
				estudiante.put("SexoEstudiante", rs.getString("SexoEstudiante"));
				
				cs2 = (CallableStatement) cn.prepareCall(query2);
			
				cs2.setString(1, codEstudiante);
				cs2.setString(2, codTipoEvaluacion);
				cs2.setString(3, codProfesor);
				cs2.setString(4, codCurso);
				cs2.setInt(5, idSeccion);
				
				ResultSet rs2 = cs2.executeQuery();
				listaNotas = new ArrayList<Hashtable<String, Object>>();
				while(rs2.next()) {
					nota = new Hashtable<String, Object>();
					nota.put("IdEvaluacion", rs2.getInt("IdEvaluacion"));
					nota.put("DescripEvaluacion", rs2.getString("DescripEvaluacion"));
					nota.put("Nota", rs2.getDouble("Nota"));
					nota.put("Trimestre", rs2.getInt("Trimestre"));
					
					listaNotas.add(nota);
				}
				estudiante.put("Notas", listaNotas);
				lista.add(estudiante);
			}
		
		
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}

		return lista;
	}

	@Override
	public Hashtable<String, Object> registrarTipoEvaluacion(String descripTipoEvaluacion, int porcentaje, String codCurso) {
		Hashtable<String, Object> resultado = new Hashtable<String, Object>();
		String query = "{call usp_tipo_evaluacion_registrar(?,?,?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
		
			cs.setString(1, descripTipoEvaluacion);
			cs.setInt(2, porcentaje);
			cs.setString(3, codCurso);
			
			
			boolean registrado = cs.executeUpdate()>0;
			
			if(registrado) {
				resultado.put("resultado", "¡Tipo de evaluacion registrado exitosamente!");
			}else {
				resultado.put("resultado", "No se pudo registrar el tipo de evaluacion");				
			}
			
		} catch (SQLException e) {
			resultado.put("resultado", "No se pudo registrar tipo de evaluacion : "+e.getMessage());
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			resultado.put("resultado", "No se pudo registrar tipo de evaluacion : "+e.getMessage());
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return resultado;
	}

	@Override
	public ArrayList<Hashtable<String, Object>> recordTrimestral(String codEstudiante, int trimestre) {
		ArrayList<Hashtable<String, Object>> lista = new ArrayList<Hashtable<String, Object>>();
		Hashtable<String, Object> curso = null;
		String query = "{call usp_evaluacion_recordTrimestral(?,?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
		
			cs.setString(1, codEstudiante);
			cs.setInt(2, trimestre);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				curso = new Hashtable<String, Object>();
				curso.put("DescripCurso", rs.getString("DescripCurso"));
				curso.put("PromedioTrimestral", rs.getDouble("PromedioTrimestral"));
				
				lista.add(curso);
			}
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
			
		return lista;
	}

	@Override
	public ArrayList<Hashtable<String, Object>> recordTotal(String codEstudiante) {
		ArrayList<Hashtable<String, Object>> lista = new ArrayList<Hashtable<String, Object>>();
		Hashtable<String, Object> curso = null;
		String query = "{call usp_evaluacion_recordTotal(?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
		
			cs.setString(1, codEstudiante);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				curso = new Hashtable<String, Object>();
				curso.put("DescripCurso", rs.getString("DescripCurso"));
				curso.put("ImagenCurso", rs.getString("ImagenCurso"));	
				curso.put("Primer", rs.getDouble("Primer"));
				curso.put("Segundo", rs.getDouble("Segundo"));
				curso.put("Tercero", rs.getDouble("Tercero"));
				curso.put("PromedioFinal", rs.getDouble("PromedioFinal"));
				
				lista.add(curso);
			}
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
			
		return lista;
	}

	@Override
	public ArrayList<Hashtable<String, Object>> promedioPorCriterio(String codEstudiante, String codCurso) {
		ArrayList<Hashtable<String, Object>> lista = new ArrayList<Hashtable<String, Object>>();
		Hashtable<String, Object> evaluacion = null;
		String query = "{call usp_evaluacion_promedioPorCriterio(?,?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			
			cs.setString(1, codEstudiante);
			cs.setString(2, codCurso);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				evaluacion = new Hashtable<String, Object>();
				evaluacion.put("CodTipoEvaluacion", rs.getString("CodTipoEvaluacion"));
				evaluacion.put("DescripTipoEvaluacion", rs.getString("DescripTipoEvaluacion"));
				evaluacion.put("Primer", rs.getString("Primer"));
				evaluacion.put("Segundo", rs.getString("Segundo"));
				evaluacion.put("Tercero", rs.getString("Tercero"));
				
				lista.add(evaluacion);
			}
			
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return lista;
	}

	@Override
	public ArrayList<Hashtable<String, Object>> detalleNota(String codEstudiante, String codCurso, int trimestre,String codTipoEvaluacion) {
		ArrayList<Hashtable<String, Object>> lista = new ArrayList<Hashtable<String, Object>>();
		Hashtable<String, Object> evaluacion = null;
		String query = "{call usp_evaluacion_detalleNota(?,?,?,?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			
			cs.setString(1, codEstudiante);
			cs.setString(2, codCurso);
			cs.setInt(3, trimestre);
			cs.setString(4, codTipoEvaluacion);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				evaluacion = new Hashtable<String, Object>();
				evaluacion.put("IdEvaluacion", rs.getInt("IdEvaluacion"));
				evaluacion.put("DescripEvaluacion", rs.getString("DescripEvaluacion"));
				evaluacion.put("nota", rs.getDouble("nota"));
				
				lista.add(evaluacion);
			}
			
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return lista;
	}

	@Override
	public ArrayList<Hashtable<String, Object>> promedioPorCriterioTrimestral(String codEstudiante, String codCurso, int trimestre) {
		ArrayList<Hashtable<String, Object>> lista = new ArrayList<Hashtable<String, Object>>();
		Hashtable<String, Object> evaluacion = null;
		String query = "{call usp_evaluacion_promedioPorCriterioTrimestral(?,?,?)}";
		Connection cn = Conexion.getInstance().getConnection();
		//System.out.println("iniciando peticion");
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			
			cs.setString(1, codEstudiante);
			cs.setString(2, codCurso);
			cs.setInt(3, trimestre);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				evaluacion = new Hashtable<String, Object>();
				evaluacion.put("CodTipoEvaluacion", rs.getString("CodTipoEvaluacion"));
				evaluacion.put("DescripTipoEvaluacion", rs.getString("DescripTipoEvaluacion"));
				evaluacion.put("Promedio", rs.getString("Promedio"));
				
				lista.add(evaluacion);
			}
			
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		//System.out.println("finalizando peticion");
		return lista;
	}

}
