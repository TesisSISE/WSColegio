package com.colegio.pe.dao;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.colegio.pe.dao.interfaces.IEstudianteDAO;
import com.colegio.pe.util.Conexion;

import java.sql.Connection;

public class EstudianteDAO implements IEstudianteDAO {
	private static final Logger log = LogManager.getLogger(EstudianteDAO.class);
	
	@Override
	public Hashtable<String,Object> registrarEstudiante(Hashtable<String,Object> estudiante) {
		Hashtable<String,Object> resultado = new Hashtable<String,Object>();
		Connection cn = Conexion.getInstance().getConnection();
		String query = "{call usp_estudiante_registrar(?,?,?,?,?,?)}";
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			
			cs.setString(1, estudiante.get("nomEstudiante").toString());
			cs.setString(2, estudiante.get("apePaternoEstudiante").toString());
			cs.setString(3, estudiante.get("apeMaternoEstudiante").toString());			
			cs.setString(4, estudiante.get("fechaNacEstudiante").toString());
			cs.setString(5, estudiante.get("sexoEstudiante").toString());
			cs.setString(6, estudiante.get("dniEstudiante").toString());
			
			boolean registrado = cs.executeUpdate()>0;
			
			if(registrado) {
				resultado.put("titulo","Registrado");
				resultado.put("mensaje","Estudiante registrado correctamente!");
			}else {
				resultado.put("titulo","Error");
				resultado.put("mensaje","No se pudo registrar!");
			}
			
		} catch (SQLException e) {
			resultado.put("titulo","Error");
			resultado.put("mensaje",e.getMessage());
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			resultado.put("titulo","Error");
			resultado.put("mensaje",e.getMessage());
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return resultado;
	}

	@Override
	public ArrayList<Hashtable<String,Object>> listarEstudiantes() {
		ArrayList<Hashtable<String,Object>> lista = new ArrayList<Hashtable<String,Object>>();
		Connection cn = Conexion.getInstance().getConnection();
		String query = "{call usp_estudiante_listar()}";
		Hashtable<String,Object> matricula = null;
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			
			ResultSet rs = cs.executeQuery();
		
			while(rs.next()) {
				matricula = new Hashtable<String,Object>();
				Hashtable<String,Object> estudiante = new Hashtable<String,Object>();
				estudiante.put("codEstudiante",rs.getString("CodEstudiante"));
				estudiante.put("nomEstudiante",rs.getString("NomEstudiante"));
				estudiante.put("apePaternoEstudiante",rs.getString("ApePaternoEstudiante"));
				estudiante.put("apeMaternoEstudiante",rs.getString("ApeMaternoEstudiante"));
				estudiante.put("fechaNacEstudiante",rs.getDate("FechaNacEstudiante"));
				Character sexo =rs.getString("SexoEstudiante")==null?null:rs.getString("SexoEstudiante").charAt(0);
				estudiante.put("sexoEstudiante",sexo);
				estudiante.put("dniEstudiante",rs.getString("DniEstudiante"));
				//foto de estudiante
				
				Hashtable<String,Object> seccion = new Hashtable<String,Object>();
				seccion.put("idSeccion",rs.getInt("IdSeccion"));
				seccion.put("turno",rs.getString("Turno"));
				seccion.put("nomSeccion",rs.getString("NomSeccion").charAt(0));
				seccion.put("fechaCreacion",rs.getDate("FechaCreacion"));
				seccion.put("estadoSeccion",rs.getString("EstadoSeccion"));
				//aula
				
				Hashtable<String,Object> grado = new Hashtable<String,Object>();
				grado.put("codGrado",rs.getString("CodGrado"));
				grado.put("descripGrado",rs.getString("DescripGrado"));
				
				Hashtable<String,Object> matriculaId = new Hashtable<String,Object>();
				matriculaId.put("codEstudiante", estudiante.get("codEstudiante"));
				matriculaId.put("idSeccion", seccion.get("idSeccion"));
				
				seccion.put("grado",grado);
				matricula.put("estudiante",estudiante);
				matricula.put("seccion",seccion);
				matricula.put("matriculaId",matriculaId);
				
				estudiante.put("EstadoEstudiante", rs.getBoolean("EstadoEstudiante"));
				lista.add(matricula);
			}
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return lista;
	}

	@Override
	public ArrayList<Hashtable<String,Object>> listarCursos(String codEstudiante) {
		ArrayList<Hashtable<String,Object>> lista = new  ArrayList<Hashtable<String,Object>>();
		Hashtable<String,Object> curso = null;
		String query = "{call usp_estudiante_listarCursos(?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
		
			cs.setString(1, codEstudiante);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				curso = new Hashtable<String,Object>();
				curso.put("CodCurso",rs.getString("CodCurso"));
				curso.put("DescripCurso",rs.getString("DescripCurso"));
				curso.put("ImagenCurso", rs.getString("ImagenCurso"));
				curso.put("EstadoCurso", rs.getBoolean("EstadoCurso"));
				
				lista.add(curso);
			}
		
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return lista;
	}

	@Override
	public ArrayList<Hashtable<String,Object>> listarProfesores(String codEstudiante) {
		ArrayList<Hashtable<String,Object>> lista = new ArrayList<Hashtable<String,Object>>();
		String query = "{call usp_estudiante_listarProfesores(?)}";
		Connection cn = Conexion.getInstance().getConnection();
		Hashtable<String,Object> profesor=null;
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
		
			cs.setString(1, codEstudiante);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				profesor = new Hashtable<String,Object>();
				profesor.put("CodProfesor", rs.getString("CodProfesor"));
				profesor.put("NomProfesor", rs.getString("NomProfesor"));
				profesor.put("ApePaternoProfesor", rs.getString("ApePaternoProfesor"));
				profesor.put("ApeMaternoProfesor", rs.getString("ApeMaternoProfesor"));
				profesor.put("SexoProfesor", rs.getString("SexoProfesor").charAt(0));
				profesor.put("EmailProfesor", rs.getString("EmailProfesor"));
				profesor.put("TelfProfesor", rs.getString("TelfProfesor"));
				profesor.put("FotoProfesor", rs.getString("FotoProfesor")==null?"":rs.getString("FotoProfesor"));
				//profesor.put("EstadoProfesor", rs.getBoolean("EstadoProfesor"));
				
				lista.add(profesor);
			}
			
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		
		return lista;
	}

	@Override
	public ArrayList<Hashtable<String,Object>> listarEstudiantesFull() {
		ArrayList<Hashtable<String,Object>> listaEstudiantes = new ArrayList<Hashtable<String,Object>>();
		String query = "{call usp_estudiante_listarFull()}";
		Connection cn = Conexion.getInstance().getConnection();
		Hashtable<String,Object> estudiante = null;
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				estudiante = new Hashtable<String,Object>();
				estudiante.put("codEstudiante",rs.getString("CodEstudiante"));
				estudiante.put("nomEstudiante",rs.getString("NomEstudiante"));
				estudiante.put("apePaternoEstudiante",rs.getString("ApePaternoEstudiante"));
				estudiante.put("apeMaternoEstudiante",rs.getString("ApeMaternoEstudiante"));
				estudiante.put("fechaNacEstudiante",rs.getDate("FechaNacEstudiante"));
				Character sexo =rs.getString("SexoEstudiante")==null?null:rs.getString("SexoEstudiante").charAt(0);
				estudiante.put("sexoEstudiante",sexo);
				estudiante.put("dniEstudiante",rs.getString("DniEstudiante"));
				estudiante.put("FotoEstudiante", rs.getShort("FotoEstudiante"));
				estudiante.put("EstadoEstudiante", rs.getBoolean("EstadoEstudiante"));
				
				listaEstudiantes.add(estudiante);
			}
		
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		return listaEstudiantes;
	}

	@Override
	public ArrayList<Hashtable<String, Object>> listarEstudiantesPrueba() {
		
		ArrayList<Hashtable<String,Object>> lista = new ArrayList<Hashtable<String,Object>>();
		Hashtable<String,Object> estudiante;
		String query = "{call usp_estudiante_listarFull()}";
		Connection cn = Conexion.getInstance().getConnection();
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				estudiante = new Hashtable<String,Object>();
				estudiante.put("codEstudiante", rs.getString("CodEstudiante"));
				estudiante.put("nomEstudiante", rs.getString("NomEstudiante"));
				estudiante.put("apePaternoEstudiante", rs.getString("ApePaternoEstudiante"));
				estudiante.put("apeMaternoEstudiante", rs.getString("ApeMaternoEstudiante"));
				estudiante.put("fechaNacEstudiante", rs.getDate("FechaNacEstudiante").toString());
				
				Character sexo =rs.getString("SexoEstudiante")==null?null:rs.getString("SexoEstudiante").charAt(0);
				estudiante.put("sexoEstudiante", sexo.toString());
				estudiante.put("dniEstudiante", rs.getString("DniEstudiante"));
				estudiante.put("FotoEstudiante", rs.getShort("FotoEstudiante"));
				estudiante.put("EstadoEstudiante", rs.getBoolean("EstadoEstudiante"));
								
				lista.add(estudiante);
			}
		
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		return lista;
	}

	@Override
	public ArrayList<Hashtable<String, Object>> listarCursosWeb(String codEstudiante) {
		ArrayList<Hashtable<String, Object>> cursos = new ArrayList<Hashtable<String, Object>>();
		Hashtable<String, Object> curso = null;
		String query = "{call usp_estudiante_listarCursos(?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
		
			cs.setString(1, codEstudiante);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				curso = new Hashtable<String, Object>();
				curso.put("CodCurso",rs.getString("CodCurso"));
				curso.put("DescripCurso",rs.getString("DescripCurso"));
				curso.put("CodGrado", rs.getString("CodGrado"));
				curso.put("DescripGrado", rs.getString("DescripGrado"));
				curso.put("NombresProfesor", rs.getString("NombresProfesor"));
				curso.put("EstadoCurso", rs.getBoolean("EstadoCurso"));
				
				cursos.add(curso);
			}
		
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return cursos;
	}

	@Override
	public Hashtable<String, Object> buscarPorDNI(String dniEstudiante) {
		Hashtable<String, Object> estudiante = null;
		String query = "{call usp_estudiante_buscarPorDNI(?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
		
			cs.setString(1, dniEstudiante);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				estudiante = new Hashtable<String, Object>();
				estudiante.put("CodEstudiante",rs.getString("CodEstudiante"));
				estudiante.put("NomEstudiante",rs.getString("NomEstudiante"));
				estudiante.put("ApePaternoEstudiante", rs.getString("ApePaternoEstudiante"));
				estudiante.put("ApeMaternoEstudiante", rs.getString("ApeMaternoEstudiante"));
				estudiante.put("FechaNacEstudiante", rs.getString("FechaNacEstudiante"));
				estudiante.put("SexoEstudiante", rs.getString("SexoEstudiante"));
				estudiante.put("DniEstudiante", rs.getString("DniEstudiante"));
				estudiante.put("FotoEstudiante", rs.getString("FotoEstudiante"));
				estudiante.put("EstadoEstudiante", rs.getBoolean("EstadoEstudiante"));
				
			
			}
		
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return estudiante;
	}

	@Override
	public ArrayList<Hashtable<String, Object>> listarPorSeccion(int idSeccion) {
		ArrayList<Hashtable<String, Object>> lista = new ArrayList<Hashtable<String, Object>>();
		Hashtable<String, Object> estudiante = null;
		String query = "{call usp_estudiante_listarPorSeccion(?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
		
			cs.setInt(1, idSeccion);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				estudiante = new Hashtable<String, Object>();
				estudiante.put("CodEstudiante",rs.getString("CodEstudiante"));
				estudiante.put("NomEstudiante",rs.getString("NomEstudiante"));
				estudiante.put("ApePaternoEstudiante", rs.getString("ApePaternoEstudiante"));
				estudiante.put("ApeMaternoEstudiante", rs.getString("ApeMaternoEstudiante"));
				estudiante.put("SexoEstudiante", rs.getString("SexoEstudiante"));
				estudiante.put("EstadoEstudiante", rs.getBoolean("EstadoEstudiante"));
				
				lista.add(estudiante);
			}
		
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return lista;
	}

	@Override
	public Hashtable<String, Object> modificarEstudiante(Hashtable<String, Object> estudiante) {
		Hashtable<String,Object> resultado = new Hashtable<String,Object>();
		Connection cn = Conexion.getInstance().getConnection();
		String query = "{call usp_estudiante_modificar(?,?,?,?,?,?,?,?)}";
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			
			cs.setString(1, estudiante.get("codEstudiante").toString());
			cs.setString(2, estudiante.get("nomEstudiante").toString());
			cs.setString(3, estudiante.get("apePaternoEstudiante").toString());
			cs.setString(4, estudiante.get("apeMaternoEstudiante").toString());			
			cs.setString(5, estudiante.get("fechaNacEstudiante").toString());
			cs.setString(6, estudiante.get("sexoEstudiante").toString());
			cs.setString(7, estudiante.get("dniEstudiante").toString());
			cs.setString(8, estudiante.get("fotoEstudiante").toString());
			
			boolean registrado = cs.executeUpdate()>0;
			
			if(registrado) {
				resultado.put("titulo","Modificado");
				resultado.put("mensaje","Estudiante modificado correctamente!");
			}else {
				resultado.put("titulo","Error");
				resultado.put("mensaje","No se pudo modificar!");
			}
			
		} catch (SQLException e) {
			resultado.put("titulo","Error");
			resultado.put("mensaje",e.getMessage());
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			resultado.put("titulo","Error");
			resultado.put("mensaje",e.getMessage());
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return resultado;
	}

	@Override
	public Hashtable<String, Object> eliminarEstudiante(String codEstudiante, boolean activado) {
		Hashtable<String,Object> resultado = new Hashtable<String,Object>();
		Connection cn = Conexion.getInstance().getConnection();
		String query = "{call usp_estudiante_eliminar(?,?)}";
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			
			cs.setString(1, codEstudiante);
			cs.setBoolean(2, activado);
			boolean actualizado = cs.executeUpdate()>0;
			
			if(actualizado) {
				if(activado) {
					resultado.put("resultado","Activado");
				}else {
					resultado.put("resultado","Desactivado");
				}
			}else {
				resultado.put("resultado","No se pudo eliminar!");
			}
			
		} catch (SQLException e) {
			resultado.put("resultado", e);
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			resultado.put("resultado", e);
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return resultado;
	}
	
}
