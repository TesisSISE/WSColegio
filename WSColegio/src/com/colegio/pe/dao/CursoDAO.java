package com.colegio.pe.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.colegio.pe.dao.interfaces.ICursoDAO;
import com.colegio.pe.util.Conexion;

import java.sql.CallableStatement;
import java.sql.Connection;

public class CursoDAO implements ICursoDAO{
	private static final Logger log = LogManager.getLogger(CursoDAO.class);
	
	
	@Override
	public ArrayList<Hashtable<String, Object>> listarCursos() {
		ArrayList<Hashtable<String, Object>> lista = new ArrayList<Hashtable<String, Object>>();
		Hashtable<String, Object> curso = null;
		String query = "{call usp_curso_listar()}";
		Connection cn = Conexion.getInstance().getConnection();
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				curso = new Hashtable<String, Object>();
				curso.put("CodCurso",rs.getString("CodCurso"));
				curso.put("DescripCurso",rs.getString("DescripCurso"));
				curso.put("ImagenCurso",rs.getString("ImagenCurso")==null?"":rs.getString("ImagenCurso"));
				curso.put("CodGrado",rs.getString("CodGrado"));
				curso.put("EstadoCurso",rs.getBoolean("EstadoCurso"));
					
				lista.add(curso);
			}
		
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
				
		return lista;
	}

	@Override
	public Hashtable<String, Object> registrarCurso(Hashtable<String, Object> curso) {
		Hashtable<String, Object> resultado = new Hashtable<String, Object>();
		String query = "{call usp_curso_registrar(?,?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			
			cs.setString(1, curso.get("codGrado").toString());
			cs.setString(2, curso.get("descripCurso").toString());
			
			boolean registrado = cs.executeUpdate()>0;
		
			if(registrado) {
				resultado.put("resultado", "¡Curso registrado!");
			}else {
				resultado.put("resultado", "El curso no se pudo registrar");
			}
			
		} catch (SQLException e) {
			resultado.put("resultado", "No se pudo registrar el curso : "+e.getMessage());
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			resultado.put("resultado", "No se pudo registrar el curso : "+e.getMessage());
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		
		return resultado;
	}

	@Override
	public ArrayList<Hashtable<String, Object>> listarCursosSecundaria() {
		ArrayList<Hashtable<String, Object>> lista = new ArrayList<Hashtable<String, Object>>();
		Hashtable<String, Object> curso = null;
		String query = "{call usp_curso_listarSecundaria()}";
		Connection cn = Conexion.getInstance().getConnection();
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
		
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				curso = new Hashtable<String, Object>();
				curso.put("CodCurso",rs.getString("CodCurso"));
				curso.put("DescripCurso",rs.getString("DescripCurso"));
				curso.put("EstadoCurso", rs.getBoolean("EstadoCurso"));
				
				lista.add(curso);
			}
			
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}	
		return lista;
	}

	@Override
	public Hashtable<String, Object> modificarCurso(Hashtable<String, Object> curso) {
		Hashtable<String, Object> resultado = new Hashtable<String, Object>();
		String query = "{call usp_curso_modificar(?,?,?,?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			
			cs.setString(1, curso.get("codCurso").toString());
			cs.setString(2, curso.get("descripCurso").toString());
			cs.setString(3, curso.get("codGrado").toString());
			cs.setString(4, curso.get("imagenCurso").toString());
			
			boolean modificado = cs.executeUpdate()>0;
		
			if(modificado) {
				resultado.put("resultado", "¡Curso modificado!");
			}else {
				resultado.put("resultado", "El curso no se pudo modificar");
			}
			
		} catch (SQLException e) {
			resultado.put("resultado", "No se pudo modificar el curso : "+e.getMessage());
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			resultado.put("resultado", "No se pudo modificar el curso : "+e.getMessage());
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		
		return resultado;
	}

	@Override
	public Hashtable<String, Object> eliminarCurso(String codCurso, boolean activado) {
		Hashtable<String, Object> resultado = new Hashtable<String, Object>();
		String query = "{call usp_curso_eliminar(?,?)}";
		Connection cn = Conexion.getInstance().getConnection();
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			
			cs.setString(1, codCurso);
			cs.setBoolean(2, activado);
			
			boolean actualizado = cs.executeUpdate()>0;
		
			if(actualizado) {
				if(activado) {
					resultado.put("resultado", "Activado");
				}else {
					resultado.put("resultado", "Desactivado");
				}
				
			}else {
				resultado.put("resultado", "El curso no se pudo eliminar");
			}
			
		} catch (SQLException e) {
			resultado.put("resultado", "No se pudo registrar el eliminar : "+e.getMessage());
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			resultado.put("resultado", "No se pudo registrar el curso : "+e.getMessage());
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		
		return resultado;
	}

	
}
