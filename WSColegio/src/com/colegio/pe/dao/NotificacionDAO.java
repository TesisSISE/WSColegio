package com.colegio.pe.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.colegio.pe.dao.interfaces.INotificacionDAO;
import com.colegio.pe.util.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;

public class NotificacionDAO implements INotificacionDAO{
	private static final Logger log = LogManager.getLogger(NotificacionDAO.class);
	
	@Override
	public ArrayList<Hashtable<String, Object>> listarNotificaciones() {
		ArrayList<Hashtable<String, Object>> lista = new ArrayList<Hashtable<String, Object>>();
		Hashtable<String, Object> notificacion=null;
		
		Connection cn = Conexion.getInstance().getConnection();
		String query = "{call usp_notificacion_listar()}";
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				notificacion = new Hashtable<String, Object>();
				
				notificacion.put("IdNotificacion", rs.getInt("IdNotificacion"));
				notificacion.put("IdEvaluacion", rs.getInt("IdEvaluacion"));
				notificacion.put("CodProfesor", rs.getString("CodProfesor"));
				notificacion.put("CodApoderado", rs.getString("CodApoderado"));
				notificacion.put("CodEstudiante", rs.getString("CodEstudiante"));
				notificacion.put("Mensaje", rs.getString("Mensaje"));
				notificacion.put("FechaNotificacion", rs.getString("FechaNotificacion"));
				notificacion.put("EstadoNotificacion", rs.getString("EstadoNotificacion"));	
				
				lista.add(notificacion);
			}
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		return lista;
	}

	@Override
	public ArrayList<Hashtable<String, Object>> listarNotificacionesPorApoderado(String codApoderado) {
		ArrayList<Hashtable<String, Object>> lista = new ArrayList<Hashtable<String, Object>>();
		Hashtable<String, Object> notificacion=null;
		
		Connection cn = Conexion.getInstance().getConnection();
		String query = "{call usp_notificacion_listarPorApoderado(?)}";
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
			
			cs.setString(1, codApoderado);
			
			ResultSet rs = cs.executeQuery();
			
			while(rs.next()) {
				notificacion = new Hashtable<String, Object>();
				
				notificacion.put("IdNotificacion", rs.getInt("IdNotificacion"));
				notificacion.put("IdEvaluacion", rs.getInt("IdEvaluacion"));
				notificacion.put("CodProfesor", rs.getString("CodProfesor"));
				notificacion.put("CodApoderado", rs.getString("CodApoderado"));
				notificacion.put("CodEstudiante", rs.getString("CodEstudiante"));
				notificacion.put("Mensaje", rs.getDate("Mensaje"));
				notificacion.put("FechaNotificacion", rs.getString("FechaNotificacion"));
				notificacion.put("EstadoNotificacion", rs.getString("EstadoNotificacion"));	
				
				lista.add(notificacion);
			}
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		return lista;
	}

}
