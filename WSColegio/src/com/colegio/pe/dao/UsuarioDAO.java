package com.colegio.pe.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.colegio.pe.dao.interfaces.IUsuarioDAO;
import com.colegio.pe.util.Conexion;

import java.sql.CallableStatement;
import java.sql.Connection;




public class UsuarioDAO implements IUsuarioDAO{
	private static final Logger log = LogManager.getLogger(UsuarioDAO.class);
	
	public ArrayList<Hashtable<String, Object>> listarUsuarios(){
		ArrayList<Hashtable<String, Object>> lista = new ArrayList<Hashtable<String, Object>>();
		Hashtable<String, Object> usuario=null;
		Connection cn =Conexion.getInstance().getConnection();
		try {
			Statement st = (Statement) cn.createStatement();
			ResultSet rs = st.executeQuery("select * from usuario");
			
			while(rs.next()) {
				usuario = new Hashtable<String, Object>();
				usuario.put("CodUsuario",rs.getString("CodUsuario"));
				usuario.put("NomUsuario",rs.getString("NomUsuario"));
				usuario.put("ClaveUsuario",rs.getString("ClaveUsuario"));
				usuario.put("CodTipoUsuario",rs.getString("CodTipoUsuario"));
				usuario.put("EstadoUsuario",rs.getString("EstadoUsuario"));
				
				lista.add(usuario);
			}
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return lista;
		
	}
//	

	@Override
	public Hashtable<String, Object> login(String nombreUsuario, String clave) {
		Hashtable<String, Object> usuario = null;
		Connection cn = Conexion.getInstance().getConnection();
		try {
			String query="{call usp_usuario_login(?,?)}";
			CallableStatement cs =  (CallableStatement) cn.prepareCall(query);
			
			cs.setString(1, nombreUsuario);
			cs.setString(2, clave);
			
			
			ResultSet rs = cs.executeQuery();
			
			if(rs.next()) {
				usuario = new Hashtable<String, Object>();
				usuario.put("codUsuario",rs.getString("CodUsuario"));
				usuario.put("nomUsuario",rs.getString("NomUsuario"));
				usuario.put("claveUsuario",rs.getString("ClaveUsuario"));		
				usuario.put("estadoUsuario",rs.getString("EstadoUsuario"));
				
				Hashtable<String, Object> tipoUsuario = new Hashtable<String, Object>();
				tipoUsuario.put("codTipoUsuario",rs.getString("CodTipoUsuario"));
				tipoUsuario.put("descripTipoUsuario",rs.getString("DescripTipoUsuario"));
				
				usuario.put("tipoUsuario",tipoUsuario);
				
				if(tipoUsuario.get("descripTipoUsuario").toString().equalsIgnoreCase("Director")) {
					Hashtable<String, Object> director = new Hashtable<String, Object>();
					director.put("codDirector",rs.getString("CodDirector"));
					director.put("nomDirector",rs.getString("NomDirector"));
					director.put("apePaternoDirector",rs.getString("ApePaternoDirector"));
					director.put("apeMaternoDirector",rs.getString("ApeMaternoDirector"));
					director.put("dniDirector",rs.getString("DniDirector"));
					
					usuario.put("director",director);

				}
				else if(tipoUsuario.get("descripTipoUsuario").toString().equalsIgnoreCase("Profesor")) {
					Hashtable<String, Object> profesor = new Hashtable<String, Object>();
					profesor.put("codProfesor",rs.getString("CodProfesor"));
					profesor.put("nomProfesor",rs.getString("NomProfesor"));
					profesor.put("apePaternoProfesor",rs.getString("ApePaternoProfesor"));
					profesor.put("apeMaternoProfesor",rs.getString("ApeMaternoProfesor"));
					profesor.put("sexoProfesor",rs.getString("SexoProfesor").charAt(0));
					profesor.put("dirProfesor",rs.getString("DirProfesor"));
					profesor.put("emailProfesor",rs.getString("EmailProfesor"));
					profesor.put("telfProfesor",rs.getString("TelfProfesor"));
					profesor.put("fechaNacProfesor",rs.getDate("FechaNacProfesor"));
					
					usuario.put("profesor",profesor);

				}
				else if(tipoUsuario.get("descripTipoUsuario").toString().equalsIgnoreCase("Apoderado")) {
					Hashtable<String, Object> apoderado = new Hashtable<String, Object>();
					apoderado.put("codApoderado",rs.getString("CodApoderado"));
					apoderado.put("nomApoderado",rs.getString("NomApoderado"));
					apoderado.put("apePaternoApoderado",rs.getString("ApePaternoApoderado"));
					apoderado.put("apeMaternoApoderado",rs.getString("ApeMaternoApoderado"));
					apoderado.put("dirApoderado",rs.getString("DirApoderado"));
					apoderado.put("telfApoderado",rs.getString("TelfApoderado"));
					apoderado.put("fechaNacApoderado",rs.getDate("FechaNacApoderado"));
					apoderado.put("dniApoderado",rs.getString("DniApoderado"));
					apoderado.put("sexoApoderado",rs.getString("SexoApoderado"));
					apoderado.put("fotoApoderado", rs.getString("FotoApoderado")==null?"":rs.getString("FotoApoderado"));
					usuario.put("apoderado",apoderado);
				}		
				
			}
		} catch (SQLException e) {
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		return usuario;
	}

	@Override
	public Hashtable<String, Object> cambiarClave(String codUsuario,String claveUsuario, String nuevaClaveusuario) {
		Hashtable<String, Object> resultado = new Hashtable<String, Object>();
		String query = "{call usp_usuario_cambiarClave(?,?,?)}";
		Connection cn  = Conexion.getInstance().getConnection();
		
		try {
			CallableStatement cs = (CallableStatement) cn.prepareCall(query);
		
			cs.setString(1, codUsuario);
			cs.setString(2, claveUsuario);
			cs.setString(3, nuevaClaveusuario);
			
			boolean actualizado = cs.executeUpdate()>0;
			
			if(actualizado) {
				resultado.put("resultado", "¡Su contraseña se ha cambiado!");
			}else {
				resultado.put("resultado", "la clave actual es incorrecta");
			}
			
		} catch (SQLException e) {
			resultado.put("resultado", "No se pudo cambiar la clave : "+e.getMessage());
			log.debug("error SQL: ", e);
			e.printStackTrace();
		} catch (Exception e) {
			resultado.put("resultado", "No se pudo cambiar la clave : "+e.getMessage());
			log.debug("error generico: ", e);
			e.printStackTrace();
		}finally {
			try {
				if(cn!=null) {
					cn.close();
				}
			} catch (SQLException e) {
				log.debug("No se pudo cerrar la conexion: ", e);
				e.printStackTrace();
			}
		}
		
		return resultado;
	}

	
}
