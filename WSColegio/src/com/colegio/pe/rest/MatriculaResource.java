package com.colegio.pe.rest;

import java.util.Hashtable;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.spi.AsynchronousResponse;

import com.colegio.pe.dao.MatriculaDAO;
import com.colegio.pe.rest.interfaces.IMatriculaResource;

public class MatriculaResource implements IMatriculaResource {
	private static final MatriculaDAO dao;
	
	static {
		dao = new MatriculaDAO();
	}
	
	@Override
	public void matricularEstudiante(final AsynchronousResponse response, final Hashtable<String, Object> estudiante) throws Exception{
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               Hashtable<String, Object> resultado=dao.matricularEstudiante(estudiante);
	               Response jaxrs = Response.ok(resultado).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}

}
