package com.colegio.pe.rest;

import java.util.Hashtable;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.spi.AsynchronousResponse;

import com.colegio.pe.dao.MovilDAO;
import com.colegio.pe.rest.interfaces.IMovilResource;

public class MovilResource implements IMovilResource{
	private static final MovilDAO dao;
	
	static {
		dao = new MovilDAO();
	}
	@Override
	public void registrarMovil(final AsynchronousResponse response, final Hashtable<String, Object> movil) throws Exception {
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               Hashtable<String, Object> resultado=dao.registrarMovil(movil);
	               Response jaxrs = Response.ok(resultado).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}

}
