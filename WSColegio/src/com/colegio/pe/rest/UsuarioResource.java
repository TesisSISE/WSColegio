package com.colegio.pe.rest;

import java.util.ArrayList;
import java.util.Hashtable;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.spi.AsynchronousResponse;

import com.colegio.pe.dao.UsuarioDAO;
import com.colegio.pe.rest.interfaces.IUsuarioResource;


public class UsuarioResource implements IUsuarioResource{
	private static final UsuarioDAO dao;
	
	static {
		dao = new UsuarioDAO();
	}
	
	@Override
	public void listarUsuarios(final AsynchronousResponse response) throws Exception{
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               ArrayList<Hashtable<String, Object>> lista=dao.listarUsuarios();
	               Response jaxrs = Response.ok(lista).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}

	@Override
	public void login(final AsynchronousResponse response, final String nombreUsuario, final String clave) throws Exception{
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               Hashtable<String, Object> resultado=dao.login(nombreUsuario, clave);
	               Response jaxrs = Response.ok(resultado).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}


	@Override
	public void cambiarClave(final AsynchronousResponse response, final String codUsuario, final String claveUsuario, final String nuevaClaveusuario) {
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               Hashtable<String, Object> resultado=dao.cambiarClave(codUsuario, claveUsuario, nuevaClaveusuario);
	               Response jaxrs = Response.ok(resultado).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}

	
}
