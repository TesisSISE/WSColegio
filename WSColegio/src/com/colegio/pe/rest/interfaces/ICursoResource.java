package com.colegio.pe.rest.interfaces;

import java.util.Hashtable;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.annotations.Suspend;
import org.jboss.resteasy.spi.AsynchronousResponse;


@Path("/curso")
public interface ICursoResource {
	
	@GET
	@Path("/listar")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void listarCursos(
			@Suspend(10000) AsynchronousResponse response) throws Exception;
	
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void registrarCurso(@Suspend(10000) AsynchronousResponse response, Hashtable<String, Object> curso) throws Exception;
	


	@GET
	@Path("/listarSecundaria")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void listarCursosSecundaria(
			@Suspend(10000) AsynchronousResponse response) throws Exception;

	
	@PUT
	@Path("/modificar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void modificarCurso(
			@Suspend(10000) AsynchronousResponse response,
			Hashtable<String, Object> curso) throws Exception;
	
	
	@DELETE
	@Path("/eliminar")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void eliminarCurso(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("codCurso") String codCurso,
			@QueryParam("activado") boolean activado) throws Exception;
	
}
