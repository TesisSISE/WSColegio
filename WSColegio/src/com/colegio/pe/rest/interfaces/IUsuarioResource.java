package com.colegio.pe.rest.interfaces;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.annotations.Suspend;
import org.jboss.resteasy.spi.AsynchronousResponse;


@Path("/usuario")
public interface IUsuarioResource {
	
	@GET
	@Path("/listar")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	public void listarUsuarios(
			@Suspend(10000) AsynchronousResponse response) throws Exception;
	
	
	@POST
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	public void login(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("nombreUsuario") String nombreUsuario,
			@QueryParam("clave") String clave) throws Exception;
	
	
	@PUT
	@Path("/cambiarClave")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void cambiarClave(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("codUsuario") String codUsuario, 
			@QueryParam("claveUsuario") String claveUsuario, 
			@QueryParam("nuevaClaveusuario") String nuevaClaveusuario) throws Exception;
}
