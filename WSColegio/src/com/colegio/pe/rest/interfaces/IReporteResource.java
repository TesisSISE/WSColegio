package com.colegio.pe.rest.interfaces;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.annotations.Suspend;
import org.jboss.resteasy.spi.AsynchronousResponse;

@Path("/reporte")
public interface IReporteResource {
	
	@GET
	@Path("/aprobadosPorSeccion")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void aprobadosPorSeccion(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("trimestre") int trimestre, 
			@QueryParam("codProfesor") String codProfesor) throws Exception;

	@GET
	@Path("/progresoPorSeccion")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void progresoPorSeccion(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("trimestre") int trimestre, 
			@QueryParam("codProfesor") String codProfesor
			) throws Exception;
}
