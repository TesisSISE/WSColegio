package com.colegio.pe.rest.interfaces;

import java.util.Hashtable;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.annotations.Suspend;
import org.jboss.resteasy.spi.AsynchronousResponse;


@Path("/estudiante")
public interface IEstudianteResource {
	
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void registrarEstudiante(
			@Suspend(10000) AsynchronousResponse response,
			Hashtable<String,Object> estudiante) throws Exception;
	
	@GET
	@Path("/listar")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void listarEstudiantes(
			@Suspend(10000) AsynchronousResponse response) throws Exception;
	
	
	@GET
	@Path("/listarCursos")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void listarCursos(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("codEstudiante") String codEstudiante) throws Exception;

	@GET
	@Path("/listarProfesores")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void listarProfesores(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("codEstudiante") String codEstudiante) throws Exception;

	@GET
	@Path("/listarEstudiantes")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void listarEstudiantesFull(
			@Suspend(10000) AsynchronousResponse response) throws Exception;
	
	
	@GET
	@Path("/listarPruebas")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void listarEstudiantesPrueba(
			@Suspend(10000) AsynchronousResponse response) throws Exception;
	
	
	@GET
	@Path("/listarCursosWeb")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void listarCursosWeb(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("codEstudiante") String codEstudiante) throws Exception;


	@GET
	@Path("/buscarPorDNI")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void buscarPorDNI(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("dniEstudiante") String dniEstudiante) throws Exception;


	@GET
	@Path("/listarPorSeccion")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void listarPorSeccion(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("idSeccion") int idSeccion) throws Exception;

	@PUT
	@Path("/modificarEstudiante")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void modificarEstudiante(
			@Suspend(10000) AsynchronousResponse response,
			Hashtable<String,Object> estudiante) throws Exception;
	
	
	@DELETE
	@Path("/eliminar")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void eliminarEstudiante(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("codEstudiante") String codEstudiante, 
			@QueryParam("activado") boolean activado) throws Exception;
}
