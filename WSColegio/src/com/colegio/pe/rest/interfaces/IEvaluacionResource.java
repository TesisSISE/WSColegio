package com.colegio.pe.rest.interfaces;

import java.util.ArrayList;
import java.util.Hashtable;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.annotations.Suspend;
import org.jboss.resteasy.spi.AsynchronousResponse;


@Path("/evaluacion")
public interface IEvaluacionResource {

	@GET
	@Path("/listarTipoEvaluacionPorCurso")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void listarTipoEvaluacionPorCurso(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("codCurso") String codCurso) throws Exception;



	@POST
	@Path("/registrarNotas")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void registrarNotas(
			@Suspend(10000) AsynchronousResponse response,
			ArrayList<Hashtable<String, Object>> notas) throws Exception;

	
	@GET
	@Path("/listarNotasPorDetalleCriterio")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void listarNotasPorDetalleCriterio(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("idSeccion") int idSeccion, 
			@QueryParam("codCurso")	String codCurso, 
			@QueryParam("codProfesor") String codProfesor, 
			@QueryParam("codTipoEvaluacion") String codTipoEvaluacion) throws Exception;


	@POST
	@Path("/registrarTipoEvaluacion")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void registrarTipoEvaluacion(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("descripTipoEvaluacion") String descripTipoEvaluacion, 
			@QueryParam("porcentaje") int porcentaje, 
			@QueryParam("codCurso") String codCurso) throws Exception;

	@GET
	@Path("/recordTrimestral")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void recordTrimestral(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("codEstudiante") String codEstudiante, 
			@QueryParam("trimestre") int trimestre) throws Exception;
	
	@GET
	@Path("/recordTotal")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void recordTotal(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("codEstudiante") String codEstudiante
			) throws Exception;
	
	
	@GET
	@Path("/promedioPorCriterio")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void promedioPorCriterio(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("codEstudiante") String codEstudiante, 
			@QueryParam("codCurso") String codCurso) throws Exception;
	
	@GET
	@Path("/promedioPorCriterioTrimestral")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void promedioPorCriterioTrimestral(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("codEstudiante") String codEstudiante, 
			@QueryParam("codCurso") String codCurso, 
			@QueryParam("trimestre") int trimestre) throws Exception;
	
	
	@GET
	@Path("/detalleNota")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void detalleNota(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("codEstudiante") String codEstudiante, 
			@QueryParam("codCurso") String codCurso, 
			@QueryParam("trimestre") int trimestre, 
			@QueryParam("codTipoEvaluacion") String codTipoEvaluacion) throws Exception;
}
