package com.colegio.pe.rest.interfaces;

import java.util.Hashtable;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.annotations.Suspend;
import org.jboss.resteasy.spi.AsynchronousResponse;

@Path("/apoderado")
public interface IApoderadoResource {
	
	@GET
	@Path("/listar")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void listarApoderados(
			@Suspend(10000) AsynchronousResponse response) throws Exception;
	
	
	@GET
	@Path("/listarEstudiantes")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void listarEstudiantes(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("codApoderado") String codApoderado) throws Exception;


	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void registrarApoderado(
			@Suspend(10000) AsynchronousResponse response,
			Hashtable<String, Object> apoderado) throws Exception;

	
	@POST
	@Path("/asociarEstudiante")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void asociarEstudiante(
			@Suspend(10000) AsynchronousResponse response,
			Hashtable<String, Object> estudiante) throws Exception;




	@PUT
	@Path("/modificarApoderado")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void modificarApoderado(
			@Suspend(10000) AsynchronousResponse response,
			Hashtable<String, Object> apoderado) throws Exception;
	
	@DELETE
	@Path("/eliminarApoderado")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void eliminarApoderado(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("codApoderado") String codApoderado, 
			@QueryParam("activado") boolean activado) throws Exception;
}
