package com.colegio.pe.rest.interfaces;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.annotations.Suspend;
import org.jboss.resteasy.spi.AsynchronousResponse;

@Path("/notificacion")
public interface INotificacionResource {
	
	@GET
	@Path("/listar")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void listarNotificaciones(
			@Suspend(10000) AsynchronousResponse response) throws Exception;
	
	
	
	@GET
	@Path("/listarPorApoderado")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void listarNotificacionesPorApoderado(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("codApoderado") String codApoderado) throws Exception;
}
