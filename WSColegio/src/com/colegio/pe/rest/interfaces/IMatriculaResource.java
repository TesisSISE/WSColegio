package com.colegio.pe.rest.interfaces;

import java.util.Hashtable;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.annotations.Suspend;
import org.jboss.resteasy.spi.AsynchronousResponse;


@Path("/matricula")
public interface IMatriculaResource {

	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void matricularEstudiante(
			@Suspend(10000) AsynchronousResponse response,
			Hashtable<String, Object> estudiante) throws Exception;
}
