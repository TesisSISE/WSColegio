package com.colegio.pe.rest.interfaces;

import java.util.Hashtable;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.annotations.Suspend;
import org.jboss.resteasy.spi.AsynchronousResponse;

@Path("/director")
public interface IDirectorResource {
	
	@GET
	@Path("/listarCursos")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void listarCursos(
			@Suspend(10000) AsynchronousResponse response) throws Exception;
	
	
	
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void registrarDirector(
			@Suspend(10000) AsynchronousResponse response,
			Hashtable<String,Object> director) throws Exception;


	@POST
	@Path("/aperturarSeccion")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void aperturarSeccion(
			@Suspend(10000) AsynchronousResponse response,
			Hashtable<String,Object> seccion) throws Exception;
}
