package com.colegio.pe.rest.interfaces;

import java.util.ArrayList;
import java.util.Hashtable;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.annotations.Suspend;
import org.jboss.resteasy.spi.AsynchronousResponse;

@Path("/profesor")
public interface IProfesorResource {
	
	@GET
	@Path("/listar")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void listarProfesores(
			@Suspend(10000) AsynchronousResponse response) throws Exception;
	
	
	@POST
	@Path("/registrar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void registrarProfesor(
			@Suspend(10000) AsynchronousResponse response,
			Hashtable<String,Object> profesor) throws Exception;
	
	
	@GET
	@Path("/listarSecciones")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void listarSecciones(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("codProfesor") String codProfesor) throws Exception;


	@GET
	@Path("/listarCursosDeSeccion")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void listarCursosDeSeccion(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("idSeccion") int idSeccion,
			@QueryParam("codProfesor") String codProfesor
			) throws Exception;
	
	@POST
	@Path("/asociarCurso")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void asociarCurso(
			@Suspend(10000) AsynchronousResponse response,
			ArrayList<Hashtable<String,Object>> profesores) throws Exception;


	@POST
	@Path("/asignarSeccion")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void asignarSeccion(
			@Suspend(10000) AsynchronousResponse response,
			Hashtable<String,Object> profesor) throws Exception;



	@GET
	@Path("/listarAlumnosPorSeccion")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void listarAlumnos(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("idSeccion") int idSeccion) throws Exception;
	
	@GET
	@Path("/listarCursosImpartidos")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void listarCursosImpartidos(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("codProfesor") String codProfesor, 
			@QueryParam("nivel") String nivel) throws Exception;
	
	@PUT
	@Path("/modificarProfesor")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void modificarProfesor(
			@Suspend(10000) AsynchronousResponse response,
			Hashtable<String,Object> profesor) throws Exception;
	
	@DELETE
	@Path("/eliminarProfesor")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	void eliminarProfesor(
			@Suspend(10000) AsynchronousResponse response,
			@QueryParam("codProfesor") String codProfesor, 
			@QueryParam("activado") boolean activado) throws Exception;
}
