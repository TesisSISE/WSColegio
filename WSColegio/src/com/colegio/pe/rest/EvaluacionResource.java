package com.colegio.pe.rest;

import java.util.ArrayList;
import java.util.Hashtable;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.spi.AsynchronousResponse;

import com.colegio.pe.dao.EvaluacionDAO;
import com.colegio.pe.rest.interfaces.IEvaluacionResource;

public class EvaluacionResource implements IEvaluacionResource{
	
	private static final EvaluacionDAO dao;
	
	static {
		dao = new EvaluacionDAO();
	}
	
	@Override
	public void listarTipoEvaluacionPorCurso(final AsynchronousResponse response, final String codCurso) throws Exception{
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               ArrayList<Hashtable<String, Object>> lista=dao.listarTipoEvaluacionPorCurso(codCurso);
	               Response jaxrs = Response.ok(lista).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}

	@Override
	public void registrarNotas(final AsynchronousResponse response, final ArrayList<Hashtable<String, Object>> notas) throws Exception{
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               Hashtable<String, Object> resultado=dao.registrarNotas(notas);
	               Response jaxrs = Response.ok(resultado).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}

	@Override
	public void listarNotasPorDetalleCriterio(final AsynchronousResponse response, final int idSeccion, final String codCurso, final String codProfesor, final String codTipoEvaluacion) throws Exception{
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               ArrayList<Hashtable<String, Object>> lista=dao.listarNotasPorDetalleCriterio(idSeccion, codCurso, codProfesor, codTipoEvaluacion);
	               Response jaxrs = Response.ok(lista).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}

	@Override
	public void registrarTipoEvaluacion(final AsynchronousResponse response, final String descripTipoEvaluacion, final int porcentaje, final String codCurso) throws Exception {
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               Hashtable<String, Object> resultado=dao.registrarTipoEvaluacion(descripTipoEvaluacion, porcentaje, codCurso);
	               Response jaxrs = Response.ok(resultado).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}

	@Override
	public void recordTrimestral(final AsynchronousResponse response, final String codEstudiante, final int trimestre) throws Exception {
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               ArrayList<Hashtable<String, Object>> lista=dao.recordTrimestral(codEstudiante, trimestre);
	               Response jaxrs = Response.ok(lista).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}

	@Override
	public void recordTotal(final AsynchronousResponse response, final String codEstudiante) throws Exception{
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               ArrayList<Hashtable<String, Object>> lista=dao.recordTotal(codEstudiante);
	               Response jaxrs = Response.ok(lista).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}

	@Override
	public void promedioPorCriterio(final AsynchronousResponse response, final String codEstudiante, final String codCurso) throws Exception{
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               ArrayList<Hashtable<String, Object>> lista=dao.promedioPorCriterio(codEstudiante, codCurso);
	               Response jaxrs = Response.ok(lista).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}

	@Override
	public void detalleNota(final AsynchronousResponse response, final String codEstudiante, final String codCurso, final int trimestre, final String codTipoEvaluacion) throws Exception{
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               ArrayList<Hashtable<String, Object>> lista=dao.detalleNota(codEstudiante, codCurso, trimestre, codTipoEvaluacion);
	               Response jaxrs = Response.ok(lista).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}

	@Override
	public void promedioPorCriterioTrimestral(final AsynchronousResponse response, final String codEstudiante, final String codCurso, final int trimestre) throws Exception {
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               ArrayList<Hashtable<String, Object>> lista=dao.promedioPorCriterioTrimestral(codEstudiante, codCurso, trimestre);
	               Response jaxrs = Response.ok(lista).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();	
	}



}
