package com.colegio.pe.rest;

import java.util.ArrayList;
import java.util.Hashtable;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.spi.AsynchronousResponse;

import com.colegio.pe.dao.ReporteDAO;
import com.colegio.pe.rest.interfaces.IReporteResource;

public class ReporteResource implements IReporteResource {
	private static final ReporteDAO dao;
	
	static {
		dao = new ReporteDAO();
	}
	
	@Override
	public void aprobadosPorSeccion(final AsynchronousResponse response, final int trimestre, final String codProfesor) {
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               ArrayList<Hashtable<String, Object>> lista=dao.aprobadosPorSeccion(trimestre, codProfesor);
	               Response jaxrs = Response.ok(lista).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}

	@Override
	public void progresoPorSeccion(final AsynchronousResponse response, final int trimestre, final String codProfesor) throws Exception {
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               ArrayList<Hashtable<String, Object>> lista=dao.progresoPorSeccion(trimestre, codProfesor);
	               Response jaxrs = Response.ok(lista).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
		
	}

}
