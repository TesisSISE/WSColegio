package com.colegio.pe.rest;

import java.util.ArrayList;
import java.util.Hashtable;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.spi.AsynchronousResponse;

import com.colegio.pe.dao.NotificacionDAO;
import com.colegio.pe.rest.interfaces.INotificacionResource;

public class NotificacionResource implements INotificacionResource {
	private static final NotificacionDAO dao;
	
	static {
		dao = new NotificacionDAO();
	}
	@Override
	public void listarNotificaciones(final AsynchronousResponse response) throws Exception{
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               ArrayList<Hashtable<String, Object>> lista=dao.listarNotificaciones();
	               Response jaxrs = Response.ok(lista).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}

	@Override
	public void listarNotificacionesPorApoderado(final AsynchronousResponse response, final String codApoderado) throws Exception {
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               ArrayList<Hashtable<String, Object>> lista=dao.listarNotificacionesPorApoderado(codApoderado);
	               Response jaxrs = Response.ok(lista).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}

}
