package com.colegio.pe.rest;

import java.util.ArrayList;
import java.util.Hashtable;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.spi.AsynchronousResponse;

import com.colegio.pe.dao.ApoderadoDAO;
import com.colegio.pe.rest.interfaces.IApoderadoResource;

public class ApoderadoResource implements IApoderadoResource{
	private static final ApoderadoDAO dao;
	
	static {
		dao = new ApoderadoDAO();
	}
	
	@Override
	public void listarApoderados(final AsynchronousResponse response) throws Exception{
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               ArrayList<Hashtable<String, Object>> lista=dao.listarApoderados();
	               Response jaxrs = Response.ok(lista).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}

	@Override
	public void listarEstudiantes(final AsynchronousResponse response, final String codApoderado) throws Exception{
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               ArrayList<Hashtable<String, Object>> lista=dao.listarEstudiantes(codApoderado);
	               Response jaxrs = Response.ok(lista).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}

	@Override
	public void asociarEstudiante(final AsynchronousResponse response, final Hashtable<String, Object> estudiante) throws Exception{
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               Hashtable<String, Object> resultado=dao.asociarEstudiante(estudiante);
	               Response jaxrs = Response.ok(resultado).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}

	@Override
	public void registrarApoderado(final AsynchronousResponse response, final Hashtable<String, Object> apoderado) throws Exception{
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               Hashtable<String, Object> resultado=dao.registrarApoderado(apoderado);
	               Response jaxrs = Response.ok(resultado).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}

	@Override
	public void modificarApoderado(final AsynchronousResponse response, final Hashtable<String, Object> apoderado) throws Exception{
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               Hashtable<String, Object> resultado=dao.modificarApoderado(apoderado);
	               Response jaxrs = Response.ok(resultado).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}

	@Override
	public void eliminarApoderado(final AsynchronousResponse response, final String codApoderado, final boolean activado) throws Exception {
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               Hashtable<String, Object> resultado=dao.eliminarApoderado(codApoderado, activado);
	               Response jaxrs = Response.ok(resultado).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}

}
