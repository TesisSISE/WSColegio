package com.colegio.pe.rest;

import java.util.ArrayList;
import java.util.Hashtable;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.spi.AsynchronousResponse;

import com.colegio.pe.dao.ProfesorDAO;
import com.colegio.pe.rest.interfaces.IProfesorResource;

public class ProfesorResource implements IProfesorResource{
	private static final ProfesorDAO dao;
	
	static {
		dao = new ProfesorDAO();
	}
	@Override
	public void listarProfesores(final AsynchronousResponse response) throws Exception{
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               ArrayList<Hashtable<String, Object>> lista=dao.listarProfesores();
	               Response jaxrs = Response.ok(lista).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}


	@Override
	public void listarSecciones(final AsynchronousResponse response, final String codProfesor) throws Exception{
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               ArrayList<Hashtable<String, Object>> lista=dao.listarSecciones(codProfesor);
	               Response jaxrs = Response.ok(lista).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}

	@Override
	public void listarCursosDeSeccion(final AsynchronousResponse response, final int idSeccion, final String codProfesor) throws Exception{
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               ArrayList<Hashtable<String, Object>> lista=dao.listarCursosDeSeccion(idSeccion, codProfesor);
	               Response jaxrs = Response.ok(lista).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}


	@Override
	public void registrarProfesor(final AsynchronousResponse response, final Hashtable<String, Object> profesor) throws Exception{
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               Hashtable<String, Object> resultado=dao.registrarProfesor(profesor);
	               Response jaxrs = Response.ok(resultado).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}


	@Override
	public void asociarCurso(final AsynchronousResponse response, final ArrayList<Hashtable<String,Object>> profesores) throws Exception{
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               Hashtable<String, Object> resultado=dao.asociarCurso(profesores);
	               Response jaxrs = Response.ok(resultado).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}


	@Override
	public void asignarSeccion(final AsynchronousResponse response, final Hashtable<String, Object> profesor) throws Exception{
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               Hashtable<String, Object> resultado=dao.asignarSeccion(profesor);
	               Response jaxrs = Response.ok(resultado).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();		
	}


	@Override
	public void listarAlumnos(final AsynchronousResponse response, final int idSeccion) throws Exception {
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               ArrayList<Hashtable<String, Object>> lista=dao.listarAlumnos(idSeccion);
	               Response jaxrs = Response.ok(lista).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}


	@Override
	public void listarCursosImpartidos(final AsynchronousResponse response, final String codProfesor, final String nivel) throws Exception {
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               ArrayList<Hashtable<String, Object>> lista=dao.listarCursosImpartidos(codProfesor, nivel);
	               Response jaxrs = Response.ok(lista).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}


	@Override
	public void modificarProfesor(final AsynchronousResponse response, final Hashtable<String, Object> profesor) throws Exception{
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               Hashtable<String, Object> resultado=dao.modificarProfesor(profesor);
	               Response jaxrs = Response.ok(resultado).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}


	@Override
	public void eliminarProfesor(final AsynchronousResponse response, final String codProfesor, final boolean activado) throws Exception {
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               Hashtable<String, Object> resultado=dao.eliminarProfesor(codProfesor, activado);
	               Response jaxrs = Response.ok(resultado).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}


}
