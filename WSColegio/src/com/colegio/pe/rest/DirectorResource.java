package com.colegio.pe.rest;

import java.util.ArrayList;
import java.util.Hashtable;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.spi.AsynchronousResponse;

import com.colegio.pe.dao.DirectorDAO;
import com.colegio.pe.rest.interfaces.IDirectorResource;

public class DirectorResource implements IDirectorResource{
	private static final DirectorDAO dao;
	
	static {
		dao = new DirectorDAO();
	}
	
	@Override
	public void listarCursos(final AsynchronousResponse response) throws Exception {
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               ArrayList<Hashtable<String, Object>> lista=dao.listarCursos();
	               Response jaxrs = Response.ok(lista).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}

	@Override
	public void registrarDirector(final AsynchronousResponse response, final Hashtable<String, Object> director) throws Exception{
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               Hashtable<String, Object> resultado=dao.registrarDirector(director);
	               Response jaxrs = Response.ok(resultado).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}

	@Override
	public void aperturarSeccion(final AsynchronousResponse response, final Hashtable<String, Object> seccion) throws Exception{
		Thread t = new Thread()
	      {
	         @Override
	         public void run()
	         {      
	               Hashtable<String, Object> resultado=dao.aperturarSeccion(seccion);
	               Response jaxrs = Response.ok(resultado).type(MediaType.APPLICATION_JSON+";charset=UTF-8").build();
	               response.setResponse(jaxrs);
	         }
	      };
	      t.start();
	}

}
