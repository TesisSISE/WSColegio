package com.colegio.pe.util;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.colegio.pe.rest.ApoderadoResource;
import com.colegio.pe.rest.CursoResource;
import com.colegio.pe.rest.DirectorResource;
import com.colegio.pe.rest.EstudianteResource;
import com.colegio.pe.rest.EvaluacionResource;
import com.colegio.pe.rest.MovilResource;
import com.colegio.pe.rest.NotificacionResource;
import com.colegio.pe.rest.ProfesorResource;
import com.colegio.pe.rest.ReporteResource;
import com.colegio.pe.rest.UsuarioResource;


public class Application extends javax.ws.rs.core.Application {
	private Set<Class<?>> classes = new HashSet<Class<?>>();

	public Application() {
		classes.add(UsuarioResource.class);
		classes.add(ApoderadoResource.class);
		classes.add(EstudianteResource.class);
		classes.add(ProfesorResource.class);
		classes.add(CursoResource.class);
		classes.add(EvaluacionResource.class);
		classes.add(DirectorResource.class);
		classes.add(NotificacionResource.class);
		classes.add(MovilResource.class);
		classes.add(ReporteResource.class);
		
	}
 
	@Override
	public Set<Class<?>> getClasses() {
		return classes;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<Object> getSingletons() {
		return Collections.EMPTY_SET;
	}
}
