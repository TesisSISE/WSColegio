package com.colegio.pe.util;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Conexion {
	private static final Logger log = LogManager.getLogger(Conexion.class);	
	
	private static Conexion instance;
    
    private final String URL;
    private final String HOST;
    private final String BD;
    private final String USER;
    private final String PASSWORD;
    private final int PORT;
    
    private DataSource dataSource;
    
    private Conexion() {
        HOST="localhost";
        PORT=3306;
        BD="bdcolegio";
        URL="jdbc:mysql://"+HOST+":"+Integer.toString(PORT)+"/"+BD+"?autoReconnect=true&useSSL=false";
        USER="root";
        PASSWORD="gunbound";
        
        createPoolConnections();
    }
    
    private void createPoolConnections() {
    		if(dataSource==null) {
    			BasicDataSource basicDataSource = new BasicDataSource();
                basicDataSource.setDriverClassName("com.mysql.jdbc.Driver");
                basicDataSource.setUsername(USER);
                basicDataSource.setPassword(PASSWORD);
                basicDataSource.setUrl(URL);
                basicDataSource.setMaxActive(30);
                basicDataSource.setMaxIdle(30);
                basicDataSource.setRemoveAbandoned(true);
                basicDataSource.setRemoveAbandonedTimeout(5000);
                basicDataSource.setMaxWait(6000);
                
                dataSource = basicDataSource;
    		}	
    }
    
    public static Conexion getInstance(){
        if(instance==null) instance=new Conexion();
        return instance;
    }
    
    public Connection getConnection(){
    	Connection cn = null;
        try {
			cn = (Connection) dataSource.getConnection();
		} catch (SQLException ex) {
			log.debug("Error al obtener una conexion del pool: ", ex);
			ex.printStackTrace();
		}
        return cn;
    }
    
    
    
}
