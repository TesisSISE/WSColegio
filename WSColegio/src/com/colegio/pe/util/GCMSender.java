package com.colegio.pe.util;

import java.io.IOException;
import java.util.Hashtable;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class GCMSender {
	private static final Logger log = LogManager.getLogger(GCMSender.class);
	
	private final String apiKey = "AIzaSyCQ_cejzIy3BOquDHzdQJndkp3l3s5yqyo";
	private final String senderID = "240634457811"; //Lo tengo por sea caso
	private final String url = "https://gcm-http.googleapis.com/gcm/send";
	
	public String sender(Hashtable<String, Object> notificacion) {
		String token = notificacion.get("Token")==null?"":notificacion.get("Token").toString();
		String message = notificacion.get("Mensaje")==null?"":notificacion.get("Mensaje").toString();
		String codUsuario = notificacion.get("CodUsuario")==null?"":notificacion.get("CodUsuario").toString();
		String codCurso = notificacion.get("CodCurso")==null?"":notificacion.get("CodCurso").toString();
		String descripCurso = notificacion.get("DescripCurso")==null?"":notificacion.get("DescripCurso").toString();
		String codEstudiante = notificacion.get("CodEstudiante")==null?"":notificacion.get("CodEstudiante").toString();
		String trimestre = notificacion.get("Trimestre")==null?"":notificacion.get("Trimestre").toString();
		String codTipoEvaluacion = notificacion.get("CodTipoEvaluacion")==null?"":notificacion.get("CodTipoEvaluacion").toString();
		
		String response = null;
		String json = "{\r\n" + 
				"  \"to\" : \""+token+"\",\r\n" + 
				"  \"data\":{\r\n" + 
				"    \"message\":\""+message+"\"\r\n" +
				", \"codusuario\":\""+codUsuario+"\""+
				", \"codcurso\":\""+codCurso+"\""+
				", \"descripcurso\":\""+descripCurso+"\""+
				", \"codEstudiante\":\""+codEstudiante+"\""+
				", \"trimestre\":\""+trimestre+"\""+
				", \"codTipoEvaluacion\":\""+codTipoEvaluacion+"\""+
				"  }\r\n" + 
				"}";
		HttpClient httpClient = HttpClients.createDefault();
		RequestBuilder requestBuilder = RequestBuilder.post().setUri(url);

		requestBuilder.addHeader("Authorization", "key="+apiKey);
		requestBuilder.addHeader("Content-Type","application/json");

		try {
			HttpEntity entity = new ByteArrayEntity(json.getBytes("UTF-8"));
			requestBuilder.setEntity(entity);
			HttpUriRequest uriRequest = requestBuilder.build();
		    HttpResponse httpResponse = httpClient.execute(uriRequest);
		    response = EntityUtils.toString(httpResponse.getEntity());
		} catch (IOException e) {
			log.debug(e);
		    e.printStackTrace();
		} catch (Exception e) {
			log.debug(e);
			e.printStackTrace();
		}
		
		
		
		return response;
	}
}
